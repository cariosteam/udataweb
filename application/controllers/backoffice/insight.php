<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Insight_model');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'Insight',
            'subMenu'	 	=> 'Insight',
            'contentTable'  => $this->Insight_model->getListInsight(),
        );
        $this->load->view('backoffice/insight/list_insight',$data);
    }
    public function create()
    {
        $data = array(
            'pageTitle' => 'Tambah Insight',
            'subMenu' => 'Tambah Insight',
        );
        $this->load->view('backoffice/insight/add_insight', $data);

    }
    public function insert(){
            $id = $this->input->post('id');
            $data = array(
                'CAT'					=> $this->input->post('kategori'),
                'NEWS_TITLE'			=> $this->input->post('judul'),
                'SUB_TITLE'			    => $this->input->post('sub_title'),
                'NEWS_CONTENT'			=> $this->input->post('konten_berita'),
                'NEWS_PICTURE_CAPTION'	=> $this->input->post('caption'),
                'NEWS_PICTURE_SOURCE'	=> $this->input->post('source'),
                'TAGS'					=> $this->input->post('word_tags'),
                'META_TITLE'			=> $this->input->post('judul'),
                'META_DESC'				=> $this->input->post('meta_desc'),
                'META_KEY'				=> $this->input->post('meta_key')
            );
        $data['ALIAS'] = $this->input->post('alias');
        //Upload Image
        $config = array(
            'file_name'   => $data['ALIAS'].'_'.date('YmdHis'),
            'upload_path' => "./assets/img/news",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "20480000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "1024",
            'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('camuflase', $error);
        }

        else {
            $upData = $this->upload->data();
            $file_name = $upData['file_name'];
            $data['NEWS_PICTURE'] = $file_name;
        }

        if($id != ''){
            //$data['UPDATED_BY'] 	= $this->session->userdata('USERNAME');
            $data['UPDATED_DATE'] 	= date('Y-m-d H:i:s');
            $data['CREATED_BY'] = 'admin';
            $this->Insight_model->updateNews($data, $id);

            //if($_FILES['news_picture']['name'] != ''){
            //redirect('backoffice/news/croping/'.$id);
            //}
        }else{
            //$data['CREATED_BY'] 	= $this->session->userdata('USERNAME');
            $data['CREATED_DATE'] 	= date('Y-m-d H:i:s');
            $lastInsertedId = $this->Insight_model->insertNews($data);

            //if($_FILES['news_picture']['name'] != ''){
            //	redirect('backoffice/news/croping/'.$lastInsertedId);
            //}
            $this->session->set_flashdata('pesan', 'Data insight berhasil disimpan.');
            redirect('backoffice/insight/create');
            
        }

        }



}
