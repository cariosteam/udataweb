<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }
    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'Data Testimoni',
            'subMenu'	 	=> 'data testimoni',
        );
        $this->load->view('backoffice/testimoni/list_testi',$data);
    }

    public function create(){
        $data = array(
            'pageTitle' 	=> 'Tambah Testimoni',
            'subMenu'	 	=> 'tambah testimoni',
        );
        $this->load->view('backoffice/testimoni/add_testi',$data);
    }
}
