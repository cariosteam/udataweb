<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'Kategori',
            'subMenu'	 	=> 'kategori',
        );
        $this->load->view('backoffice/cat/list_cat',$data);
    }
    public function create(){
        $data = array(
            'pageTitle' 	=> 'Tambah Kategori',
            'subMenu'	 	=> 'tambah kategori',
        );
        $this->load->view('backoffice/cat/add_cat',$data);
    }
}
