<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model');

    }
    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'Data Produk',
            'subMenu'	 	=> 'data produk',
            'contentTable'  => $this->Product_model->getListProduct(),
        );
        $this->load->view('backoffice/produk/list_produk',$data);
    }

    public function create(){
        $data = array(
            'pageTitle' 	=> 'Tambah Produk',
            'subMenu'	 	=> 'tambah produk',
        );
        $this->load->view('backoffice/produk/add_produk',$data);
    }

    public function save(){
        $id = $this->input->post('id');
        $data = array(
            'CAT'					=> $this->input->post('kategori'),
            'NAMA_PRODUK'			=> $this->input->post('produk'),
            'KONTEN'			    => $this->input->post('konten')
        );

        //Upload Image
        $config = array(
            'file_name'   => 'banner_product'.'_'.date('YmdHis'),
            'upload_path' => "./assets/css/img/klien",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "20480000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "1024",
            'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('main_banner')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('camuflase', $error);
        }

        else {
            $upData = $this->upload->data();
            $file_name = $upData['file_name'];
            $data['MAIN_BANNER'] = $file_name;
            if($id != ''){
                //$data['UPDATED_BY'] 	= $this->session->userdata('USERNAME');
                $data['UPDATED_DATE'] 	= date('Y-m-d H:i:s');
                $data['CREATED_BY'] = 'admin';

                //if($_FILES['news_picture']['name'] != ''){
                //redirect('backoffice/news/croping/'.$id);
                //}
            }else{
                //$data['CREATED_BY'] 	= $this->session->userdata('USERNAME');
                $data['CREATED_AT'] 	= date('Y-m-d H:i:s');
                $lastInsertedId = $this->Product_model->insertProduct($data);

                //if($_FILES['news_picture']['name'] != ''){
                //	redirect('backoffice/news/croping/'.$lastInsertedId);
                //}
                $this->session->set_flashdata('pesan', 'Data insight berhasil disimpan.');
                redirect('backoffice/product/create');

            }
        }
    }
}
