<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Client_model');

    }
    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'Data Klien',
            'subMenu'	 	=> 'data klien',
            'contentTable'  => $this->Client_model->getListKlien(),
        );
        $this->load->view('backoffice/klien/list_klien' ,$data);
    }

    public function create(){
        $data = array(
            'pageTitle' 	=> 'Tambah Klien',
            'subMenu'	 	=> 'tambah klien',
        );
        $this->load->view('backoffice/klien/add_klien',$data);
    }

    public function save(){
        $id = $this->input->post('id');
        $data = array(
            'CAT'					=> $this->input->post('kategori'),
            'NAMA_KLIEN'			=> $this->input->post('perusahaan'),
            'DESKRIPSI'			    => $this->input->post('desc')
        );
        //Upload Image
        $config = array(
            'file_name'   => 'logo'.'_'.date('YmdHis'),
            'upload_path' => "./assets/css/img/klien",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "20480000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "1024",
            'max_width' => "1024"
        );
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('logo_perusahaan')) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('camuflase', $error);
        }

        else {
            $upData = $this->upload->data();
            $file_name = $upData['file_name'];
            $data['PICTURES'] = $file_name;
            if($id != ''){
                //$data['UPDATED_BY'] 	= $this->session->userdata('USERNAME');
                $data['UPDATED_DATE'] 	= date('Y-m-d H:i:s');
                $data['CREATED_BY'] = 'admin';
                $this->Insight_model->updateNews($data, $id);

                //if($_FILES['news_picture']['name'] != ''){
                //redirect('backoffice/news/croping/'.$id);
                //}
            }else{
                //$data['CREATED_BY'] 	= $this->session->userdata('USERNAME');
                $data['CREATED_AT'] 	= date('Y-m-d H:i:s');
                $lastInsertedId = $this->Client_model->insertKlien($data);

                //if($_FILES['news_picture']['name'] != ''){
                //	redirect('backoffice/news/croping/'.$lastInsertedId);
                //}
                $this->session->set_flashdata('pesan', 'Data insight berhasil disimpan.');
                redirect('backoffice/client/create');

            }
        }

    }
}
