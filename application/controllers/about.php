<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'TENTANG KAMI',

        );
        $this->load->view('about_us/tentang-kami',$data);
    }
}
