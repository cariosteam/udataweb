<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'PRODUK',
        );
        $this->load->view('product/produk',$data);
    }
    public function churn_prediction(){
        $this->load->view('product/churnPredict/churn_prediction');
    }

    public function gis_analitics(){
        $this->load->view('product/gisAnalitics/gis_analitics');
    }
}
