<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Insight_model');
    }

    public function index()
    {
        $data = array(
            'pageTitle' 	=> 'KABAR TERBARU ',
            'contentInsight' => $this->Insight_model->getAllInsight(),
        );
        $this->load->view('insight/lastest-insight',$data);
    }

    public function detail($id){
        $data = array(
            'pageTitle' 	=> 'DETAIL INSIGHT',
            'detailInsight' => $this->Insight_model->getInsightById($id),
        );
        $this->load->view('insight/detail-insight',$data);
    }

}
