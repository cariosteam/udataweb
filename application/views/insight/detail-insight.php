<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/components/readMore.css" rel="stylesheet">
<section class="portofolio">
    <div class="author">
        <a href="#" class="name">
            <img src="<?= base_url()?>assets/css/img/actor/swat.png" class="circle" height="50" width="50"/>
            <br/> Admin Udata
        </a>
        <p>Some bio text that goes over several lines so that we have some text that can be used for showing how it works.</p>
        <p>And another line just to show it in greater detail.</p>
    </div>
    <article>
        <?php foreach ($detailInsight as $content){?>
        <div class="article-content">
            <div class="row">
                    <h2><?= $content->NEWS_TITLE; ?></h2>
                    <img src="<?= base_url()?>assets/img/news/<?= $content->NEWS_PICTURE; ?>" class="img-responsive">
                    <?= $content->NEWS_CONTENT;?>
            </div>
        </div>
        <?php } ?>
        <br>
        <br>
        <div class="comment">
            <div class="wrapper">
                <div class="commentBoxfloat">
                    <form id="cmnt">
                        <fieldset>
                            <div class="form_grp">
                                <label>comment</label>
                                <textarea id="userCmnt" placeholder="Write your comment here. You can Edit and Delete options. Just Hover in Your comment, you see the both buttons"></textarea>
                            </div>
                            <div class="form_grp">
                                <button type="button" id="submit">Submit</button>
                            </div>
                        </fieldset>
                    </form>
                </div>

                <div id="cmntContr"></div>

            </div>
        </div>
    </article>
</section>
<?php $this->load->view('foot/footer_home'); ?>
<script src="<?= base_url()?>assets/js/components/detailInsight.js"></script>