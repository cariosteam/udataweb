<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/components/insight.css" rel="stylesheet">
<link href="<?= base_url()?>assets/css/lib/treeSource.css" rel="stylesheet">
<link href="<?= base_url()?>assets/css/lib/styleNews.css" rel="stylesheet">
<section id="portfolio" class="content">
    <!-- Page Content -->
    <hr class="star-primary">
    <div class="container">
        <div class="col-xs-12">
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">
               <h3>KABAR TERBARU</h3>
                <?php foreach ($contentInsight as $content){ ?>
                <a href="#">
                <div class="content column col-xs-12 col-sm-6 col-md-6">
                    <!-- Post-->
                    <div class="post-module">
                        <!-- Thumbnail-->
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day"><?= date("d",strtotime($content->CREATED_DATE));?></div>
                                <div class="month"><?= substr(date("F",strtotime($content->CREATED_DATE)),0,3);?></div>
                            </div><img src="<?= base_url()?>assets/img/news/<?= $content->NEWS_PICTURE; ?>" class="" >
                        </div>
                        <!-- Post Content-->
                        <div class="post-content">
                            <div class="category"><a href="<?= site_url()?>insight/detail/<?= $content->NEWS_ID; ?>" style="color: white">Selengkapnya</a></div>
                            <h1 class="title"><?= $content->NEWS_TITLE; ?></h1>
                            <h4><?= $content->SUB_TITLE; ?></h4>
                            <?php
                            $original_string = $content->NEWS_CONTENT;
                            $limited_string = character_limiter($original_string,150);
                            ?>
                            <div class="description">
                                <?= $limited_string; ?></p>
                            </div>
                            <div class="post-meta pull-right"><span class="comments"><i class="fa fa-comments"></i><a href="#"> 39 comments</a></span></div>
                        </div>
                    </div>
                </div>
                </a>
                <?php } ?>
                <!-- Pager -->

                <div class="col-md-4 col-md-offset-4">
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>
                </div>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
                <!-- Blog Categories Well -->
                    <div class="panel-body">
                        <h3>Aset Insight</h3>
                        <div class="files-tree-list undefined">
                            <button type="button" class="list-title toggle-subfiles toggle-parent-active"><span class="toggle-icon"><span class="icon fa fa-folder-open opened"></span><span class="icon fa fa-folder closed"></span></span><span class="text-holder"><span class="text">2016</span></span>
                            </button>
                            <ul class="list">
                                <li class="file files-tree-list">
                                    <button type="button" class="list-title toggle-subfiles toggle-parent-active"><span class="toggle-icon"><span class="icon fa fa-folder-open opened"></span><span class="icon fa fa-folder closed"></span></span><span class="text-holder"><span class="text">Januari (10)</span></span>
                                    </button>
                                    <ul class="list subfiles">
                                        <li class="file"><span class="text-holder"><span class="icon fa fa-file-o"></span><a href="#" class="text">File lvl 2</a></span>
                                        </li>
                                        <li class="file"><span class="text-holder"><span class="icon fa fa-file-o"></span><a href="#" class="text">File lvl 2</a></span>
                                        </li>
                                        <li class="file"><span class="text-holder"><span class="icon fa fa-file-o"></span><a href="#" class="text">File lvl 2</a></span>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                <hr>

                <!-- Side Widget Well -->
                <div class="panel-content">
                    <div class="panel-heading text-center"><h4 style="color:white;">Word Tags</h4></div>
                    <div class="panel-body">
                        <p>
                            <b>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</b>
                        </p>
                    </div>
                </div>

            </div>

        </div>
        <!-- /.row -->
        </div>

        <hr>
    </div>
    <!-- /.container -->
</section>
<?php $this->load->view('foot/footer_home'); ?>
<script type="text/javascript">
    $(window).load(function() {
        $('.post-module').hover(function() {
            $(this).find('.description').stop().animate({
                height: "toggle",
                opacity: "toggle"
            }, 300);
        });
    });
</script>
<script>
    try{Typekit.load();}catch(e){}
</script>
<script>
    $(document).on('click','.toggle-parent-active', function(){
        $(this).parent().toggleClass('active');
    })
</script>
<script src="<?= base_url()?>assets/js/lib/typeKit.js"></script>

