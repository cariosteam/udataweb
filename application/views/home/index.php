<!--<link href="--><?//= base_url()?><!--assets/css/lib/materialize.css" rel="stylesheet"/>-->
<link href="<?= base_url()?>assets/css/lib/testimoni.css" rel="stylesheet"/>
<?php $this->load->view('head/header_home'); ?>
<section id="portfolio" class="content">
    <div class="header-image">
        <div class="row">
            <div class="col-lg-12">
                <div class="full-img" style="background:url('<?= base_url()?>assets/img/banner/stock-photo-business-people-handshake-greeting-deal-concept-345349079.jpg')">
                	<div class="full-img-overlay">
                		<h2 class="full-img-text">Smart Insights to <br> Leverage Your Business</h2>
                		<button id="discover" class="full-img-button center-block">DISCOVER</button>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <div id="big-data-home" class="header-image">
        <div class="row">
            <div class="col-lg-12">
                <div class="full-img" style="background:url('<?= base_url()?>assets/img/banner/shutterstock_345678419.jpg')">
                	<div class="full-img-overlay-2">
                		<h2 class="full-img-text full-img-text-2">BIG DATA</h2>
                		<h3 class="full-img-text-desc container">
                			Big Data adalah data yang memiliki skala, distribusi dan/atau keragaman yang sangat besar, sehingga membutuhkan penggunaan arsitekstur teknikal dan metode analitik tertentu untuk mendapatkan wawasan yang memberikan nilai baru bagi bisnis.  Dalam menjalankan organisasi, sebelum big data analytics ada, seorang leader mengambil keputisan hanya berdasarkan ‘intuisi’. Kehadiran teknologi big data mengubah hal itu. Kini, para leader memanfaatkan hasil analisis data sebagai sumber informasi untuk mengambil keputusan. Bahkan, data – oriented decision making semakin menjadi kelaziman bagi para leader,. Sejumlah peneliti menyebutkan bahwa pegambilan keputusan berorientasi data mampu menghasilkan keputusan yang lebih tepat dibandingkan dengan yang mengandalkan intuisi semata.
                		</h3>
                        <form action="<?= site_url()?>about/">
                		    <button class="full-img-button full-img-button-2 center-block" type="submit">SELENGKAPNYA</button>
                        </form>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <div id="layanan-kami">
        <div class="container">
			<h2 class="full-img-text-3">LAYANAN KAMI</h2>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="berita-1.html">
                        <div class="uk-panel uk-panel-box uk-panel-box-secondary">
                            <div class="uk-panel-teaser">
                                <img src="<?= base_url()?>assets/img/usocmed.png" alt="">
                            </div>
                            <h3 class="uk-panel-title">
                            	Fokus melakukan monitoring dan reporting dari berbagai platform media sosial, UData socmed merupakan layanan Analitycs yang tepat untuk manajemen issue dan brand image bagi perusahaan anda.
                            </h3>
                            <button class="btn center-block btn-danger">SELENGKAPNYA</button>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="berita-1.html">
                        <div class="uk-panel uk-panel-box uk-panel-box-secondary">
                            <div class="uk-panel-teaser">
                                <img src="<?= base_url()?>assets/img/ubusiness.png" alt="">
                            </div>
                            <h3 class="uk-panel-title">
								Pemanfaatan sumber data Telkom Group & Platform Big Data untuk meningkatkan kinerja bisnis pelanggan.
                            </h3>
                            <button class="btn center-block btn-danger">SELENGKAPNYA</button>
                        </div>

                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="berita-1.html">
                        <div class="uk-panel uk-panel-box uk-panel-box-secondary">
                            <div class="uk-panel-teaser">
                                <img src="<?= base_url()?>assets/img/usolution.png" alt="">
                            </div>
                            <h3 class="uk-panel-title">
								Manfaatkan data yang anda miliki dengan Solusi big data analitycs berbasis prescriptive & predictive untuk membuat transformasi di perusahaan anda.
                            </h3>
                            <button class="btn center-block btn-danger">SELENGKAPNYA</button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="why-us" class="gis-feature" data-parallax="scroll" data-image-src="<?= base_url()?>assets/img/gis-analytics.jpg">
            <div class="feature-overlay">
                <div class="container">
                    <div class="row no-gutter">
                        <div class="col-md-4">
                            <div class="feature-item">
                                <i class="fa fa-mobile-phone"></i>
                                <p>DATA FIXED & MOBILE BROADBAND</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-item">
                                <i class="fa fa-random"></i>
                                <p>KAPABILITAS MENYEDIAKAN TOTAL ATAU PARTIAL SOLUTION</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-item no-border-right">
                                <i class="fa fa-sliders"></i>
                                <p>PERUSAHAAN BERBASIS TELEKOMUNIKASI</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="samples-testi">
            <div class="container">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <figure class="snip1533">
                        <figcaption>
                            <img src="<?= base_url()?>assets/css/img/actor/detective.png" class="img-responsive img-circle center-block" width="100" height="100">
                            <blockquote>
                                <p>If you do the job badly enough, sometimes you don't get asked to do it again.</p>
                            </blockquote>
                            <h3>Wisteria Ravenclaw</h3>
                            <h4>Google Inc.</h4>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                <figure class="snip1533">
                    <figcaption>
                        <img src="<?= base_url()?>assets/css/img/actor/detective.png" class="img-responsive img-circle center-block" width="100" height="100">
                        <blockquote>
                            <p>I'm killing time while I wait for life to shower me with meaning and happiness.</p>
                        </blockquote>
                        <h3>Ursula Gurnmeister</h3>
                        <h4>Facebook</h4>
                    </figcaption>
                </figure>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                <figure class="snip1533">
                    <figcaption>
                        <img src="<?= base_url()?>assets/css/img/actor/detective.png" class="img-responsive img-circle center-block" width="100" height="100">
                        <blockquote>
                            <p>The only skills I have the patience to learn are those that have no real application in life. </p>
                        </blockquote>
                        <h3>Ingredia Nutrisha</h3>
                        <h4>Twitter</h4>
                    </figcaption>
                </figure>
                </div>
            </div>
        </div>
</section>
<?php $this->load->view('foot/footer_home'); ?>

<script src="<?= base_url()?>assets/js/lib/script.js"></script>

<script>
	$("#discover").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#big-data-home").offset().top
	    }, 1000);
	});
</script>
<style>
    .btn-social i {
        line-height: 45px !important;
    }
</style>