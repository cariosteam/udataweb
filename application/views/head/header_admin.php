<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Backoffice | <?php echo $pageTitle; ?></title>

    <link rel="icon" type="image/jpg" href="<?= base_url()?>assets/img/logo/icon.jpg">
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= base_url()?>assets/css/lib/datepicker3.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/lib/styles_admin.css" rel="stylesheet"/>
    <link href="<?= base_url()?>assets/css/lib/back/dataTables.responsive.css" rel="stylesheet"/>
    <link href="<?= base_url()?>assets/css/lib/back/dataTables.bootstrap.css" rel="stylesheet"/>
    <link href="<?= base_url()?>assets/css/lib/back/jquery.dataTables.min.css" rel="stylesheet"/>

<!--Icons-->
    <script src="<?= base_url()?>assets/js/lib/lumino.glyphs.js"></script>

    <script src="<?= base_url()?>assets/js/lib/chart-data.js"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Udata Admin</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg>Butuh Bantuan ?</a></li>
                        <li role="presentation" class="divider"></li>
                        <li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Ubah Profil</a></li>
                        <li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li class="active"><a href="index.html"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
        <li class="parent ">
            <a href="#sub-item-1" data-toggle="collapse"><span><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"/></svg></span> Insight
            </a>
            <ul class="children collapse" id="sub-item-1">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/insight/create">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Buat Insight
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/insight">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Insight
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/category">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Kategori Insight
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#"><svg class="glyph stroked app window with content"><use xlink:href="#stroked-app-window-with-content"/></svg> Pages</a>
        </li>
        <li class=""><a href="index.html"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Laporan</a></li>
        <li class="parent">
            <a href="#sub-item-3" data-toggle="collapse"><svg class="glyph stroked paper coffee cup"><use xlink:href="#stroked-paper-coffee-cup"/></svg> Klien</a>
            <ul class="children collapse" id="sub-item-3">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/client/create">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tambah Klien
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/client">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Klien
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/category">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Kategori Klien
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#sub-item-4" data-toggle="collapse"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Testimoni</a>
            <ul class="children collapse" id="sub-item-4">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/testimoni/create">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tambah Testimoni
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/testimoni">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Testimoni
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#sub-item-5" data-toggle="collapse"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Produk</a>
            <ul class="children collapse" id="sub-item-5">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/product/create">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tambah Produk
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/product">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Produk
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/category">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Kategori Produk
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#sub-item-6" data-toggle="collapse"><i class="glyphicon glyphicon-book"></i> Parent Kategori</a>
            <ul class="children collapse" id="sub-item-6">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/product/create">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tambah Parent
                    </a>
                </li>
                <li>
                    <a class="" href="<?= site_url()?>backoffice/product">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Parent Kategori
                    </a>
                </li>
            </ul>
        </li>
        <li class=""><a href="index.html"><svg class="glyph stroked email"><use xlink:href="#stroked-email"/></svg> Pesan</a></li>
        <li role="presentation" class="divider"></li>
        <li class="parent">
            <a href="#sub-item-6" data-toggle="collapse"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Administrasi</a>
            <ul class="children collapse" id="sub-item-6">
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Kunci Layar
                    </a>
                </li>
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Pengguna Backoffice
                    </a>
                </li>
            </ul>
        </li>
    </ul>

</div>

