<?php $this->load->view('head/header_home'); ?>
    <link href="<?= base_url()?>assets/css/lib/gridmos.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/lib/animated_accordion.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/components/about.css" rel="stylesheet">
    <section id="our-team" class="content">
        <div class="header-image">
        <div class="row">
                <div class="full-img" style="background:url('<?= base_url()?>assets/img/banner/stock-photo-team-teamwork-togetherness-collaboration-concept-343048862.jpg')">
                    <div class="full-img-overlay">
                        <h2 class="full-img-text">Our Team</h2>
                        <div id="our-team-img" class="row">
                            <div class="col-md-push-1 col-md-2 col-sm-push-1 col-sm-2 col-xs-4">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front" style="background:url('<?= base_url()?>assets/img/8086836-vector-profile-icon.jpg')">
                                        </div>
                                        <div class="back">
                                            <div class="team-socmed">
                                                <p class="team-name">AGUS LAKSONO</p>
                                                <p class="team-position">Head of Data Scientist</p>
                                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                                <a href="#"><i class="fa fa-twitter-square"></i></a>
                                                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-push-1 col-md-2 col-sm-push-1 col-sm-2 col-xs-4">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front" style="background:url('<?= base_url()?>assets/img/8086836-vector-profile-icon.jpg');">
                                        </div>
                                        <div class="back">
                                            <div class="team-socmed">
                                                <p class="team-name">EDWIN PURWANDESI</p>
                                                <p class="team-position">Head of Businnes Development</p>
                                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                                <a href="#"><i class="fa fa-twitter-square"></i></a>
                                                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-push-1 col-md-2 col-sm-push-1 col-sm-2 col-xs-4">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front" style="background:url('<?= base_url()?>assets/img/8086836-vector-profile-icon.jpg');">
                                        </div>
                                        <div class="back">
                                            <div class="team-socmed">
                                                <p class="team-name">IDA BAGUS MAHAPUTRA A.</p>
                                                <p class="team-position">Head of Technical</p>
                                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                                <a href="#"><i class="fa fa-twitter-square"></i></a>
                                                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-push-1 col-md-2 col-sm-push-1 col-sm-2 col-xs-4">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front" style="background:url('<?= base_url()?>assets/img/8086836-vector-profile-icon.jpg');">
                                        </div>
                                        <div class="back">
                                            <div class="team-socmed">
                                                <p class="team-name">KOMANG BUDI ARYASA</p>
                                                <p class="team-position">Project Director</p>
                                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                                <a href="#"><i class="fa fa-twitter-square"></i></a>
                                                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-push-1 col-md-2 col-sm-push-1 col-sm-2 col-xs-4">
                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front" style="background:url('<?= base_url()?>assets/img/8086836-vector-profile-icon.jpg');">
                                        </div>
                                        <div class="back">
                                            <div class="team-socmed">
                                                <p class="team-name">HARTANA</p>
                                                <p class="team-position">General Support</p>
                                                <a href="#"><i class="fa fa-facebook-square"></i></a>
                                                <a href="#"><i class="fa fa-twitter-square"></i></a>
                                                <a href="#"><i class="fa fa-linkedin-square"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div id="tagline">
            <div class="container">
                <p>
                    “Big Data, Big Opportunities”
                </p>
                <p>
                    Telkom telah menyiapkan strategi pengembangan dan implementasi Big Data, baik bagi internal perusahaan maupun untuk pelanggan.
                </p>
            </div>
        </div>

        <div id="our-skill">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <svg id="svg" style="display:block;height:500px; width:620px; margin-left:auto !important;; margin-right:auto !important;">></svg>
                    </div>
                    <div class="col-md-5">
                        <div class="panel">
                                <div class="panel-body">
                                    <div class="faq-section" style="padding: 5px;">
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#one">
                                                            BIG DATA
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="one" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Big Data adalah data dengan karakteristik ukuran besar (volume), cepat (velocity), dan bermacam – macam format (variety) yang membutuhkan proses pengolahan data yang berbeda, skill khusus dan teknologi tertentu yang berbeda dari teknologi pengolahan data seperti biasanya.
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#two">
                                                            Pengelola Repositori Data Telkom Group
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="two" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        UData mengelola repositori data Telkom Group yang dibuat anonim dan diagregasi untuk memenuhi standar privasi pelanggan yang tinggi (data governance & data policy).
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#tree">
                                                            Tim Data Analis yang Andal
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="tree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        UData memiliki expert team data analyst (data scientist) untuk memberikan insight yang lebih tajam.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <div class="container">
            <hr class="star-primary">
            <div class="row">
                <div id="wrapper" class="wrapper">
                    <div class="row">
                        <div class="col-md-8 text-left">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="faq-section" style="padding: 5px;">
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#one">
                                                            BIG DATA
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="one" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Big Data adalah data dengan karakteristik ukuran besar (volume), cepat (velocity), dan bermacam – macam format (variety) yang membutuhkan proses pengolahan data yang berbeda, skill khusus dan teknologi tertentu yang berbeda dari teknologi pengolahan data seperti biasanya.
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#two">
                                                            Pengelola Repositori Data Telkom Group
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="two" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        UData mengelola repositori data Telkom Group yang dibuat anonim dan diagregasi untuk memenuhi standar privasi pelanggan yang tinggi (data governance & data policy).
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="active" data-toggle="collapse" href="#tree">
                                                            Tim Data Analis yang Andal
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="tree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        UData memiliki expert team data analyst (data scientist) untuk memberikan insight yang lebih tajam.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel">
                                <div class="panel-body">
                                    <h3>Management Team</h3>
                                    <p>Telkom telah menyiapkan strategi pengembangan dan implementasi Big Data, baik bagi internal perusahaan maupun untuk pelanggan.</p>
                                    <div class="photo-grid clearfix">
                                        <ul class=clearfix">
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/actor/detective.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">AGUS LAKSONO</p>
                                                                <p class="team-position">Head of Data Scientist</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/actor/detective.png" alt="300x300" width="300" height="300" />-->
                                            </li>
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/quotes.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">Big Data Telkom</p>
                                                                <p class="team-position">We empower Your business</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/quotes.png" alt="300x300" width="300" height="300" />-->
                                            </li>
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/actor/firefighter.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">EDWIN PURWANDESI</p>
                                                                <p class="team-position">Head of Businnes Development</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/actor/firefighter.png" alt="300x300" width="300" height="300" />-->
                                            </li>
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/actor/journalist.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">IDA BAGUS MAHAPUTRA A.</p>
                                                                <p class="team-position">Head of Technical</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/actor/journalist.png" alt="300x300" width="300" height="300" />-->
                                            </li>
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/actor/judge.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">KOMANG BUDI ARYASA</p>
                                                                <p class="team-position">Project Director</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/actor/judge.png" alt="300x300" width="300" height="300" />                            </li>-->
                                            <li>
                                                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                                                    <div class="flipper">
                                                        <div class="front" style="background:url('<?= base_url()?>assets/css/img/actor/swat.png')">
                                                        </div>
                                                        <div class="back">
                                                            <div class="team-socmed">
                                                                <p class="team-name">HARTANA</p>
                                                                <p class="team-position">General Support</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!--                                                <img src="--><?//= base_url()?><!--assets/css/img/actor/swat.png" alt="300x300" width="300" height="300" />-->
                                            </li>
                                        </ul>
                                    </div><!-- /photo-grid -->
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div class="col-md-4 text-left">
                            <div class="row">
                                <h3>Kapabilitas Telkom</h3>
                                <div class="bar-chart primary" data-total="92" animated>
                                    <span class="bar-chart--inner" style="width:92%;"></span>
                                    <span class="bar-chart--text">Pengetahuan Bisnis 92%</span>
                                </div>

                                <div class="bar-chart secondary" data-total="82" animated>
                                    <span class="bar-chart--inner" style="width:82%;"></span>
                                    <span class="bar-chart--text">Pengelolaan Data & Teknisi Data 82%</span>
                                </div>

                                <div class="bar-chart tertiary" data-total="75" animated>
                                    <span class="bar-chart--inner" style="width:75%;"></span>
                                    <span class="bar-chart--text">Penggalian Data, Pembelajaran Mesin, Statistik 75%</span>
                                </div>

                                <div class="bar-chart forth" data-total="82" animated>
                                    <span class="bar-chart--inner" style="width:82%;"></span>
                                    <span class="bar-chart--text">Komunikasi, Manajemen Proyek, Berpikir Logis 82%</span>
                                </div>

                                <div class="bar-chart fiveth" data-total="52" animated>
                                    <span class="bar-chart--inner" style="width:52%;"></span>
                                    <span class="bar-chart--text">Pemrograman dan Data Handling 52%</span>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="polaroid">
                                    <img src="<?= base_url()?>assets/css/img/teamwork.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /wrapper --></div>
        </div>
    </section>
    <script src="<?= base_url()?>assets/js/components/about.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/snap.svg/0.3.0/snap.svg-min.js"></script>
<script>
    var programmingSkills = [
    {
      value: 20,
      label: 'Pengetahuan Bisnis',
      color: '#3399FF'
    },
    {
      value: 23,
      label: 'Pengolahan Data & Teknisi Data',
      color: '#FFC575'
    },
    {
      value: 17,
      label: 'Penggalian Data, Pembelajaran Mesin, Statistik',
      color: '#99CC00'
    },
    {
      value: 22,
      label: 'Komunikasi, Manajemen Proyek, Berpikir Logis',
      color: '#FF3300'
    },
    {
      value: 18,
      label: 'Pemrograman dan Data Handling',
      color: '#944DDB'
    },
];
</script>
<script src="<?= base_url()?>assets/js/lib/svg-donut-chart-framework.js"></script>

<?php $this->load->view('foot/footer_home')?>