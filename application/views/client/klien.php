<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/lib/testimoni.css" rel="stylesheet"/>
<link href="<?= base_url()?>assets/css/components/client.css" rel="stylesheet">

<!-- Portfolio Grid Section -->
<section id="portfolio" class="content">
    <hr class="star-primary">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3>Klien Kami</h3>
            </div>

            <div class="client-sorting">
                <ul>
                    <li class="sort-active" data-filter="all">All</li>
                    <li data-filter="1">Aviation</li>
                    <li data-filter="2">Banking</li>
                    <li data-filter="3">Energy &amp; resource</li>
                    <li data-filter="4">Goverment</li>
                    <li data-filter="5">Insurance</li>
                    <li data-filter="6">Manufacturing &amp; Agribusiness</li>
                    <li data-filter="7">Media &amp; Communication</li>
                </ul>
            </div>
        </div>

        <div class="our-client-container">
            <div class="row">
                <div class="filtr-item col-md-3 col-centered" data-category="1" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-garudaindo.png" alt="sample" class="tt-danger hover-tt-top" data-hover="Hahahaha">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-commonweath.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="2" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-bni.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-equity.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kimiafarma.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                     <img src="<?= base_url() ?>assets/img/klien/logo-kotabandung.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kab-bandung.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="4" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-kotabogor.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="1, 3" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-bandungresik.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="3" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-pertamina.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="5" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                             <img src="<?= base_url() ?>assets/img/klien/admedika.png" alt="sample">
                    </a>
                </div>
            
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/client-ptpn-x.png" alt="sample">
                    </a>
                </div>

                <div class="filtr-item col-md-3 col-centered" data-category="7" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-lin" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/Client-telkomsel.png" alt="sample">
                    </a>
                </div>
                
                <div class="filtr-item col-md-3 col-centered" data-category="6" data-sort="value">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <img src="<?= base_url() ?>assets/img/klien/logo-domino.png" alt="sample">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="col-md-8 col-md-offset-2">
                <figure class="snip1533">
                    <figcaption>
                        <img src="<?= base_url()?>assets/css/img/actor/detective.png" class="img-responsive img-circle center-block" width="100" height="100">
                        <blockquote>
                            <p>If you do the job badly enough, sometimes you don't get asked to do it again.</p>
                        </blockquote>
                        <h3>Wisteria Ravenclaw</h3>
                        <h4>Google Inc.</h4>
                    </figcaption>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
                    </div>
                </figure>
            </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.our-client-container').filterizr();
    });
</script>
<?php $this->load->view('foot/footer_home'); ?>
