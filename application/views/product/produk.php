<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/components/product.css" rel="stylesheet">
<!-- Portfolio Grid Section -->
<section id="portfolio" class="content">
    <div class="container">
        <hr class="star-primary">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3>PRODUK & LAYANAN Kami</h3>
            </div>
            <div class="row">
                <div class="col-md-3 col-centered">
                    <figure class="snip1321"><img src="<?= base_url()?>assets/css/img/Customer-Profile.jpg" alt="product1" class="img-responsive"/>
                        <figcaption>
                            <h4>SocMed</h4>
                            <p>UData Social Media</p>
                            <form action="http://www-dev.udata.id/socmed">
                                <input type="submit" class="btn btn-danger" value="Lihat SocMed">
                            </form>
                        </figcaption><a href="http://www-dev.udata.id/socmed"></a>
                    </figure>
                </div>
                <div class="col-md-3 col-centered">
                    <figure class="snip1321"><img src="<?= base_url()?>assets/css/img/Customer-Profile.jpg" alt="sq-sample26"/>
                        <figcaption>
                            <h4>Churn Prediction</h4>
                            <p>UData Churn Prediction</p>
                            <form action="<?= site_url()?>product/churn_prediction">
                                <input type="submit" class="btn btn-danger" value="Lihat Churn Prediction">
                            </form>
                        </figcaption><a href="<?= site_url()?>product/churn_prediction"></a>
                    </figure>
                </div>
                <div class="col-md-3 col-centered">
                    <figure class="snip1321"><img src="<?= base_url()?>assets/css/img/Customer-Profile.jpg" alt="sq-sample26"/>
                        <figcaption>
                            <h4>GIS Analitics</h4>
                            <p>UData GIS Analitics</p>
                            <form action="<?= site_url()?>product/gis_analitics">
                                <input type="submit" class="btn btn-danger" value="Lihat SocMed">
                            </form>
                        </figcaption><a href="<?= site_url()?>product/gis_analitics"></a>
                    </figure>
                </div>
                <div class="col-md-3 col-centered">
                    <figure class="snip1321"><img src="<?= base_url()?>assets/css/img/Customer-Profile.jpg" alt="sq-sample26"/>
                        <figcaption>
                            <h4>SocMed</h4>
                            <p>UData Social Media</p>
                            <form action="<?= site_url()?>about/">
                                <input type="submit" class="btn btn-danger" value="Lihat SocMed">
                            </form>
                        </figcaption><a href="http://google.com"></a>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('foot/footer_home'); ?>

<script type="text/javascript">
    $(document).ready(function() {
       $(".hover").mouseleave(
            function () {
                $(this).removeClass("hover");
            }
        ); 
    });
</script>