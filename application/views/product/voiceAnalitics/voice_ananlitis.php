<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/components/client.css" rel="stylesheet">

<!-- Portfolio Grid Section -->
<section id="portfolio" class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3>Churn Prediction</h3>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="<?= base_url()?>assets/img/banner/Big-Data-Slider-1.png" alt="">
            </div>
        </div>
        <br>
        <div class="churn-prediction">
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        Solusi yang dibangun untuk mencegah churn pelanggan dari suatu perusahaan akibat tingginya persaingan. Solusi ini dibangun menggunakan analitik big data untuk memprediksi pelanggan dari suatu perusahaan yang akan churn, mengenali tiga churn cause utama, serta memberikan symptom untuk menentukan caring dan actions yang dilakukan.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>FITUR</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h4>Pembayaran Tagihan</h4>
                    <p>Jumlah biling (tagihan ), pola pembayaran, metode pembayaran, tunggakan. </p>
                </div>
                <div class="col-sm-6">
                    <h4>Pola Penggunaan</h4>
                    <p>Jumlah biling (tagihan ), pola pembayaran, metode pembayaran, tunggakan. </p>
                </div>
                <div class="col-sm-6">
                    <h4>Pembayaran Tagihan</h4>
                    <p>Jumlah biling (tagihan ), pola pembayaran, metode pembayaran, tunggakan. </p>
                </div>
                <div class="col-sm-6">
                    <h4>Pola Penggunaan</h4>
                    <p>Jumlah biling (tagihan ), pola pembayaran, metode pembayaran, tunggakan. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('foot/footer_home'); ?>

<style>
    .scroll-pane {
        height: 500px;
        overflow: auto;
    }
</style>