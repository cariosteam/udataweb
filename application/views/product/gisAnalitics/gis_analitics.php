<?php $this->load->view('head/header_home'); ?>
<link href="<?= base_url()?>assets/css/lib/animated_accordion.css" rel="stylesheet">


<!-- Portfolio Grid Section -->
<section id="portfolio" class="content">
    <div class="row">
        <div class="content-header col-lg-12">
            <div class="gis-header-parallax" data-parallax="scroll" data-image-src="<?= base_url()?>assets/img/gis.jpg"></div>
        </div>
    </div>
    <div class="churn-prediction">
        <div class="gis-product-desc">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3>GIS Analytics</h3>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            SGIS (Geographic Information System) adalah : Sistem informasi khusus yang mengelola data yang memiliki informasi spasial (bereferensi keruangan). Atau dalam arti yang lebih sempit, adalah sistem komputer yang memiliki kemampuan untuk membangun, menyimpan, mengelola dan menampilkan informasi berefrensi geografis, misalnya data yang diidentifikasi menurut lokasinya, dalam sebuah database.
                        </p>
                        <p>
                            Geographic Information System dapat diakses, ditransfer, ditransformasikan, diproses dan ditampilkan dengan menggunakan berbagai macam program aplikasi perangkat lunak (software). (Sumber: http://integrasiautama.com/gis-geographic-information-system)
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>FITUR</h3>
                    <br>
                </div>
            </div>
        </div>
        <div class="gis-feature" data-parallax="scroll" data-image-src="<?= base_url()?>assets/img/gis-analytics.jpg">
            <div class="feature-overlay">
                <div class="container">
                    <div class="row no-gutter">
                        <div class="col-md-4">
                            <div class="feature-item">
                                <i class="glyphicon glyphicon-star"></i>
                                <p>Spatial Analysis</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-item">
                                <i class="glyphicon glyphicon-stats"></i>
                                <p>Mapping</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-item no-border-right">
                                <i class="glyphicon glyphicon-eye-open"></i>
                                <p>Virtualization</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="arrow-down center-block"></div>
        <div class="feature-description">
            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
        <div id="feature-description" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="container">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="feature-descripton-content">
                            <h4 class="center-block">SPATIAL ANALYSIS</h4>
                            <p class="center-block">
                            Salah satu kegunaaan teknologi, khususnya Sistem Informasi Geografis, dalam perencanaan wilayah dan kota adalah seorang perencana dapat melakukan berbagai analisa spasial. Hasil dari analisa spasial ini memiliki beragam fungsi, dari menjelaskan kondisi eksisting dari wilayah perencanaan, mencari hubungan antara satu aspek dengan aspek perencanaan lainnya, hingga menentukan peruntukan lahan yang sesuai berdasarkan karakteristik spasialnya.</p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="feature-descripton-content">
                            <h4 class="center-block">MAPPING</h4>
                            <p class="center-block">
                            Bentuk ungkapan data dan informasi geologi suatu daerah / wilayah / kawasan dengan tingkat kualitas yang tergantung pada skala peta yang digunakan dan menggambarkan informasi sebaran, jenis dan sifat batuan, umur, stratigrafi, struktur, tektonika, fisiografi dan potensi sumber daya mineral serta energi yang disajikan dalam bentuk gambar dengan warna, simbol dan corak atau gabungan ketiganya.
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="feature-descripton-content">
                            <h4 class="center-block">VIRTUALIZATION</h4>
                            <p class="center-block">
                            Melalui proses yang dikenal dengan visualisasi, GIS dapat digunakan untuk menghasilkan gambar bukan hanya peta, namun gambar animasi dan produk kartografi lainnya. Gambar ini memungkinkan peneliti melihat subyek mereka dengan cara yang belum pernah diketahui sebelumnya. Aplikasi GIS dapat dilakukan dengan media internet melalui teknologi peta server internet, dan data spasial data spasial.</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#feature-description" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#feature-description" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="container faq-section">
            <div class="row">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3>FAQ</h3>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                <a class="active" data-toggle="collapse" href="#one">
                                    Apa yang dimaksud dengan GIS Analytics?
                                </a>
                            </h4>
                            </div>
                            <div id="one" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Sistem informasi geospasial berbasis web yang memungkinan pengguna membuat data & analisa geospasial <i>multi</i> layer yang lebih jelas dan detail serta dapat memonitoring secara <i>online</i> & <i>real time</i>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                <a data-toggle="collapse" href="#two">
                                    Bagaimana Cara Menggunakanya?
                                </a>
                            </h4>
                            </div>
                            <div id="two" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Melakukan akses ke web UData (<i>dashboard</i>) dengan menggunakan PC, laptop atau tablet
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                <a class="active" data-toggle="collapse" href="#three">
                                    Apa kegunaan GIS Analytics?
                                </a>
                            </h4>
                            </div>
                            <div id="three" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Kegunaan GIS Analytics adalah sebagai berikut:
                                        <br><b>1. Mapping Potensi Market</b><br>
                                        Melakukan mapping data yang  terdiri dari data geospasial dan atribut pada platform GIS tersebut. Hasil dari mapping dapat divisualisasikan dalam bentuk tabel atau grafis.
                                        <br><b>2. Incident Analysis</b><br>
                                        Melakukan identifikasi & analisis terhadap  suatu peristiwa yang terjadi pada suatu lokasi.
                                        <br><b>3. Radius Udara & Jarak Darat Terdekat (Find Nearest)</b><br>
                                        Mendapatkan radius udara & jarak darat terdekat dari suatu lokasi
                                        <br><b>4. Segmentasi Priority</b><br>
                                        Menentukan prioritas berdasarkan segmen pelanggan atau calon pelanggannya.
                                        <br><b>5. Travelling Time (Waktu Tempuh)</b><br>
                                        Mengetahui waktu tempuh pada suatu area.
                                        <br><b>6. Best Time to Visit/Call Recommendation</b><br>
                                        Mengetahui waktu terbaik yang paling efisien untuk melakukan panggilan atau kunjungan berdasarkan data historis yang ada.
                                        <br><b>7. Product Movement</b><br>
                                        Melakukan tracking aset yang dimiliki oleh suatu perusahaan, misalnya kendaraan, pesawat, dan kapal.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                <a data-toggle="collapse" href="#four">
                                    Apa kelebihannya?
                                </a>
                            </h4>
                            </div>
                            <div id="four" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Kelebihan GIS Analytics antara lain:
                                        <br><b>1. Spatial Analysis</b></br>
                                        Berfungsi untuk mengenali suatu lokasi; mengukur suatu ukuran, bentuk, dan distribusi; hubungan antar lokasi; mengetahui lokasi dan jalur yang terbaik dan tepat; mendeteksi dan menggabungkan pola; dan pengambilan keputusan.
                                        <br><b>2. Content</b></br>
                                        Udata GIS mempunyai data Telkom Group dan demografis penduduk Indonesia yang mempunyai potensi dalam analisis untuk use case tertentu.
                                        <br><b>3. Imagery and Remote Sensing</b></br>
                                        Data imagery mempunyai ukuran yang sangat besar karena kualitasnya sangat tinggi. Udata GIS Analytics dapat meng-handle data tersebut. Selain itu, Udata GIS Analytics dapat memonitor dan mengolah citra satelit.
                                        <br><b>4. Tiga Dimensi (3D)</b></br>
                                        Selain layer 2D, Udata GIS Analytics dapat mengolah data 3D agar pengguna dapat menvisualisasikan seperti keadaan sebenarnya.
                                        <br><b>5. Computer Aided Engineering (CAD)</b></br>
                                        Data dari arsitektur bangunan atau gedung yang telah dirancang di AutoCAD atau software CAD lainnya dapat diintegrasikan pada layer Udata GIS. Pengguna dapat mengatur pencahayaan matahari di dalam fitur tersebut, untuk mengetahui lokasi mana yang terhalang bayang-bayang matahari baik pada saat pagi, siang, dan sore.
                                        <br><b>6. Community Engagement</b></br>
                                        Pengguna dapat memonitor sentimen pelanggan berdasarkan wilayah yang diambil dari data media sosial. Sehingga pengguna dapat mengetahui wilayah mana yang paling aktif di media sosial, kapan mereka aktif, jenis media sosial apa yang paling sering digunakan di wilayah tersebut, dan lain-lain. Dengan hasil tersebut, perusahaan dapat mengambil keputusan yang tepat yang sesuai dengan kondisi di masyarakat.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('foot/footer_home'); ?>