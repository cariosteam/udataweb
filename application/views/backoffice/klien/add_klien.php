<?php $this->load->view('head/header_admin'); ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?= site_url()?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active"><?php echo $subMenu; ?></li>
        </ol>
    </div><!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Form Klien Baru
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" action="<?php echo site_url();?>backoffice/client/save" enctype="multipart/form-data" method="post">
                                <div class="form-group hidden">
                                    <input class="form-control" value="" name="username">
                                </div>
                                <div class="form-group">
                                    <label>Nama Kategori</label>
                                    <input class="form-control" placeholder="Enter text" name="kategori" id="kategori" required>
                                </div>
                                <div class="form-group">
                                    <label>Nama Perusahaan</label>
                                    <input class="form-control" placeholder="Enter text" name="perusahaan" id="perusahaan" required>
                                </div>
                                <div class="form-group">
                                    <label>Logo Perusahaan</label>
                                    <input type="file" name="logo_perusahaan" required>
                                </div>
                                <div class="form-group">
                                    <label>Deskripsi Singkat</label>
                                    <textarea class="form-control" rows="10" cols="6" name="desc"></textarea>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </div>
                                </form>
                                <!-- /.col-lg-6 (nested) -->

                                <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>

        </div>
        <?php $this->load->view('foot/footer_admin'); ?>



