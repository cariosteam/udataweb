<?php $this->load->view('head/header_admin'); ?>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="<?= site_url()?>">
                    <svg class="glyph stroked home">
                        <use xlink:href="#stroked-home"></use>
                    </svg>
                </a>
            </li>
            <li class="active">
                <?php echo $subMenu; ?>
            </li>
        </ol>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Data Klien</b>
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="tables">
                            <thead>
                            <tr>
                                <th data-field="id" class="col-xs-1 text-left">No.</th>
                                <th data-field="judul" class="text-center">Nama Perusahaan</th>
                                <th data-field="penulis" class="text-center">Kategori</th>
                                <th data-field="penulis" class="text-center">Deskkripsi Singkat</th>
                                <th data-field="status" class="text-center">Logo Perusahaan</th>
                                <th data-field="aksi" class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1;?>
                                <?php foreach ($contentTable as $content){ ?>
                                <tr>
                                    <td class="text-left">
                                        <?php echo $no ?>
                                    </td>
                                    <td class="text-left">
                                        <?= $content->NAMA_KLIEN;?>
                                    </td>
                                    <td class="text-center">
                                        <?= $content ->CAT;?>
                                    </td>
                                    <td class="text-left">
                                        <?= $content->DESKRIPSI;?>
                                    </td>
                                    <td class="text-center"><img src="<?= base_url()?>assets/css/img/klien/<?= $content->PICTURES;?>" height="100" width="100"></td>
                                    <td class="text-center"><a class="btn btn-warning" href="<?php echo site_url();?>berita/edit/<??>"><i class="glyphicon glyphicon-pencil"></i></a>&nbsp;<a class="btn btn-danger" onclick="hapusdata('<?php  ?>')"><i class="glyphicon glyphicon-remove"></i></a></td>
                                </tr>
                                <?php $no=$no+1; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
    </div>
<?php $this->load->view('foot/footer_admin'); ?>