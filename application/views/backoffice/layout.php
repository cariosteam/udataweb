<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BackOffice | <?php echo $pageTitle;?></title>

    <link rel="icon" type="image/jpg" href="<?= base_url()?>assets/img/logo/icon.jpg">
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url()?>assets/css/lib/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= base_url()?>assets/css/lib/datepicker3.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/lib/styles_admin.css" rel="stylesheet"/>

    <!--Icons-->
    <script src="<?= base_url()?>assets/js/lib/lumino.glyphs.js"></script>

    <script src="<?= base_url()?>assets/js/lib/chart-data.js"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Udata Admin</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><svg class="glyph stroked eye"><use xlink:href="#stroked-eye"/></svg>Butuh Bantuan ?</a></li>
                        <li role="presentation" class="divider"></li>
                        <li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Ubah Profil</a></li>
                        <li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li class="active"><a href="index.html"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
        <li class="parent ">
            <a href="#sub-item-1" data-toggle="collapse"><span><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"/></svg></span> Insight
            </a>
            <ul class="children collapse" id="sub-item-1">
                <li>
                    <a class="" href="<?= site_url()?>backoffice/">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Buat Insight
                    </a>
                </li>
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Insight
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#sub-item-2" data-toggle="collapse"><svg class="glyph stroked app window with content"><use xlink:href="#stroked-app-window-with-content"/></svg> Pages</a>
            <ul class="children collapse" id="sub-item-2">
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Tambah
                    </a>
                </li>
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Daftar Insight
                    </a>
                </li>
            </ul>
        </li>
        <li class=""><a href="index.html"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Laporan</a></li>
        <li class=""><a href="index.html"><svg class="glyph stroked paper coffee cup"><use xlink:href="#stroked-paper-coffee-cup"/></svg> Klien</a></li>
        <li class=""><a href="index.html"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Testimoni</a></li>
        <li class=""><a href="index.html"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Produk</a></li>
        <li class=""><a href="index.html"><svg class="glyph stroked email"><use xlink:href="#stroked-email"/></svg> Pesan</a></li>
        <li role="presentation" class="divider"></li>
        <li class="parent">
            <a href="login.html"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Administrasi</a>
        </li>
    </ul>

</div>

<div id="content">
    <!-- Content -->
    <?php echo $this->load->view($content, $contentData);?>
    <!-- End of Content -->
</div>

<script src="<?= base_url()?>assets/js/lib/jquery-1.11.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url()?>assets/js/lib/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/js/lib/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="<?= base_url()?>assets/js/lib/bootstrap-datepicker.js"></script>
<script>
    $('#calendar').datepicker({
    });

    !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
            $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
    }(window.jQuery);

    $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
    })
    $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
    })
</script>
</body>

</html>


