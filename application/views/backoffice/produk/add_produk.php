<?php $this->load->view('head/header_admin'); ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="<?= site_url()?>">
                    <svg class="glyph stroked home">
                        <use xlink:href="#stroked-home"></use>
                    </svg>
                </a>
            </li>
            <li class="active">
                <?php echo $subMenu; ?>
            </li>
        </ol>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Form Produk Baru
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form role="form" enctype="multipart/form-data" action="<?php echo site_url();?>backoffice/product/save" method="post">
                                <div class="form-group">
                                    <label>Kategori Produk</label>
                                    <input class="form-control" placeholder="Enter text" name="kategori" id="kategori" required>
                                </div>
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input class="form-control" placeholder="Enter text" name="produk" id="produk" required>
                                </div>
                                <div class="form-group">
                                    <label>Main Banner</label>
                                    <input type="file" name="main_banner" required>
                                </div>
                                <div class="form-group">
                                    <label>Konten Produk</label>
                                    <textarea class="form-control" rows="10" name="konten" cols="6" id="editorpost"></textarea>
                                </div>
                                <div class="form-group pull-right">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                </div>
                                <!-- /.col-lg-6 (nested) -->

                                <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>

        </div>
        <?php $this->load->view('foot/footer_admin'); ?>
        <script type="text/javascript">
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editorpost');
        </script>