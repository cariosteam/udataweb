<?php $this->load->view('head/header_admin'); ?>
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?= site_url()?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active"><?php echo $subMenu; ?></li>
        </ol>
    </div><!--/.row-->
    <div class="row">
        <div class="col-lg-12">
            <?php echo form_open_multipart('backoffice/insight/insert');?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Form Insight Baru
                    <div class="row">
                        <h7><?php echo $this->session->flashdata('pesan')?></h7>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="form-group hidden">
<!--                                    <input class="form-control" value="" name="username">-->
                                </div>
                                <div class="form-group">
                                    <label>Kategori Insight</label>
                                    <input class="form-control" placeholder="Enter text" name="kategori" id="kategori" required>
                                </div>
                                <div class="form-group">
                                    <label>Judul Insight</label>
                                    <input class="form-control" placeholder="Enter text" name="judul" id="judul" required>
                                </div>
                                <div class="form-group">
                                    <label>Sub Judul</label>
                                    <input class="form-control" placeholder="Enter text" name="sub_title" id="sub_title" required>
                                </div>
                                <div class="form-group">
                                    <label>Alias</label>
                                    <input class="form-control" placeholder="Enter text" name="alias" id="alias" required>
                                </div>
                                <div class="form-group">
                                    <label for="textfield" class="control-label">Gambar</label>
                                    <input name="<?php echo 'userfile' ?>" type="file" size="20">
                                </div>
                                <div class="form-group">
                                    <label>Caption Gambar</label>
                                    <input class="form-control" placeholder="Enter text" name="caption" id="caption" required>
                                </div>
                                <div class="form-group">
                                    <label>Sumber Gambar</label>
                                    <input class="form-control" placeholder="Enter text" name="source" id="source" required>
                                </div>
                        <!-- /.col-lg-6 (nested) -->

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">Konten Insight</div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <textarea class="col-md-6" name="konten_berita" id="editorpost" rows="5" required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">SEO Material</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                            <div class="form-group">
                                <label>Word Tags</label>
                                <input class="form-control" placeholder="Enter text" name="word_tags" id="word_tags" required>
                            </div>
                            <div class="form-group">
                                <label>Meta Key</label>
                                <input class="form-control" placeholder="Enter text" name="meta_key" id="meta_key" required>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <input class="form-control" placeholder="Enter text" name="meta_desc" id="meta_desc" required>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" name="save" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        <?= form_close(); ?>
                    </div>
             </div>
         </div>
    </div>
<?php $this->load->view('foot/footer_admin'); ?>
<script type="text/javascript">
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editorpost' );
</script>


