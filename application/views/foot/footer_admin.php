<script src="<?= base_url()?>assets/js/lib/jquery-1.11.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url()?>assets/js/lib/bootstrap.min.js"></script>
<!-- dataTables plugin -->
<script src="<?= base_url()?>assets/js/lib/back/jquery.dataTables.min.js"></script>
<script src="<?= base_url()?>assets/js/lib/chart.min.js"></script>
<script src="js/chart-data.js"></script>
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
<script src="<?= base_url()?>assets/js/lib/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<!--<script src="--><?//= base_url()?><!--assets/js/lib/back/bootstrap-table.js"></script>-->
<!--<script>-->
<!--    $('#calendar').datepicker({-->
<!--    });-->
<!---->
<!--    !function ($) {-->
<!--        $(document).on("click","ul.nav li.parent > a > span.icon", function(){-->
<!--            $(this).find('em:first').toggleClass("glyphicon-minus");-->
<!--        });-->
<!--        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");-->
<!--    }(window.jQuery);-->
<!---->
<!--    $(window).on('resize', function () {-->
<!--        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')-->
<!--    })-->
<!--    $(window).on('resize', function () {-->
<!--        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')-->
<!--    })-->
<!--</script>-->
<script type="text/javascript">
    $(document).ready( function() {
        $('#tables').dataTable( {
            "language": {
                "zeroRecords": "Tidak ada data yang ditemukan"
            }
        });
    });
</script>
</body>

</html>
