<!-- Footer -->
<footer class="text-left">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="footer-col col-md-4">
                    <h3><img src="<?= base_url()?>assets/img/logo/udata-logo-white40.png"></h3>
                    <p></p>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Tentang Kami</h3>
                    <ul class="list-group no-style">
                        <li><a><span><i class="fa fa-angle-right"></i> Syarat dan Ketentuan</span></a></li>
                        <li><a><i class="fa fa-angle-right"></i> Kebijakan Privasi</a></li>
                        <li><a><i class="fa fa-angle-right"></i> Hubungi Kami</a></li>
                        <li><a><i class="fa fa-angle-right"></i> Tentang Kami</a></li>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3>Kontak</h3>
                    <h7>Divisi Digital Service(DDS)</h7><br>
                    <h7> Menara Multimeddia Lt. 7 Jalan Kebon Sirih No. 12 Jakarta Pusat 10110</h7><br>
                    <h7> Telepon: +62-21-3482213(office)</h7><br>
                    <h7> Email: bigdata@telkom.com</h7><br>
                    <h3>Ikuti Kami</h3>
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <span>&copy; 2016 Telkom Indonesia</span>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url()?>assets/js/lib/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>


<!-- Theme JavaScript -->
<script src="<?= base_url()?>assets/js/lib/freelancer.min.js"></script>

<!-- Materialize JS -->
<!--<script src="--><?//= base_url()?><!--assets/js/lib/materialize.js"></script>-->


<script src="<?= base_url()?>assets/js/lib/jScrollPane.js"></script>

<script src="<?= base_url()?>assets/js/lib/jquery.filterizr.min.js"></script>

<script src="<?= base_url()?>assets/js/lib/parallax.min.js"></script>

<script src="<?= base_url()?>assets/js/lib/script.js"></script>
<!--<script src="--><?//= base_url()?><!--assets/js/lib/uikit.min.js"></script>-->
</body>

</html>
