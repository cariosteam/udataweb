<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_model extends CI_Model {

    var $table = 'monitoring_tbl';
    public function __construct()
    {
        parent::__construct();
    }
    function index(){
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->result();
    }
    public function visitor_per_tahun(){
        $selecting=array(
            '*',
            'year(date) as tahun',
            'count(id) as visit',
        );
        $query=$this->db->select($selecting)->distinct()->from($this->table)->where('page_view',site_url())->or_where('page_view',site_url().'welcome/keluhan')->or_where('page_view',site_url().'welcome/berita')
            ->group_by('tahun')->get();
        return $query->result();
    }
    public function visitor_per_page_view(){
        $selecting=array(
            '*',
            'year(date) as tahun',
            'count(id) as visit',
        );
        $query=$this->db->select($selecting)->distinct()->from($this->table)
            ->where('page_view',site_url())->or_where('page_view',site_url().'welcome/keluhan')->or_where('page_view',site_url().'welcome/berita')
            ->group_by('page_view')->get();

        return $query->result();
    }
    public function total_visitor(){
        $selecting=array(
            'count(id) as asa',
        );
        $query=$this->db->select($selecting)->distinct()->from($this->table)->group_by('date')->get();
        return $query->result();
    }



}
