<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model
{

    var $table = 'x_klien';

    public function __construct()
    {
        parent::__construct();
    }

    function getListKlien()
    {
        $this->db->order_by('CREATED_AT', 'desc');
        $query = $this->db->select('*')->from($this->table)->get();
        return $query->result();
    }

    function getTopKlien()
    {
        $this->db->order_by('CREATED_DATE', 'desc');
        $query = $this->db->select('*')->from($this->table)->get();
        return $query->result();
    }

    function insertKlien($data)
    {
        $this->db->insert($this->table, $data);
    }
}
