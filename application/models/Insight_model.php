<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insight_model extends CI_Model{

    var $table='x_news';
    public function __construct(){
        parent::__construct();
    }
    function getListInsight(){
        $this->db->order_by('CREATED_DATE','desc');
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->result();
    }
    function getAllInsight(){
        $this->db->order_by('CREATED_DATE','desc');
        $this->db->limit(6);
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->result();
    }
    function insertNews($data){
        $this->db->insert($this->table,$data);
    }
    function show(){
        $this->db->order_by('created_at','desc');
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->result();
    }
    function edit($id){
        $query=$this->db->select('*')->from($this->table)->where('news_id',$id)->get();
        return $query->result();
    }
    function tambah(){
        date_default_timezone_set("Asia/Jakarta");
        $date=date('Y-m-d h:i:sa');
        $username =$this->input->post('username') ;
        $judul = $this->input->post('judul');
        $deskripsi = $this->input->post('editorpost');
        $data = array(
            'username'=>$username,
            'title_news'=>$judul,
            'news_description'=>$deskripsi,
            'created_at'=>$date);
        $this->db->insert($this->table,$data);
    }
    function hapus($id){
        $this->db->where('news_id',$id);
        $this->db->delete($this->table);
    }
    function getInsightById($id){
        $query=$this->db->select('*')->from($this->table)->where('NEWS_ID',$id)->get();
        return$query->result();
    }
    function ubah($id){
        $judul = $this->input->post('judul');
        $author = $this->input->post('username');
        $deskripsi = $this->input->post('editorpost');
        date_default_timezone_set("Asia/Jakarta");
        $date=date('Y-m-d h:i:sa');
        $data = array(
            'title_news'=>$judul,
            'username' =>$author,
            'created_at'=>$date,
            'news_description'=>$deskripsi,
        );
        $this->db->where('news_id',$id);
        $this->db->update($this->table,$data);
    }
    function jumlah_baris(){
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->num_rows();
    }
    function per_page($limited,$offset){
        if($this->input->post('carijudul')==null) {
            $this->db->order_by('created_at','desc');
            $this->db->limit($limited, $offset);
            $query = $this->db->get($this->table);
            return $query->result();
        }
        else{
            $this->db->order_by('created_at','desc');
            $this->db->like('title_news',$this->input->post('carijudul'));
            $query = $this->db->get($this->table);
            return $query->result();
        }
    }
    function beritalainnya($id){
        $this->db->limit(4);
        $query=$this->db->select('*')->from($this->table)->where('news_id!=',$id)->get();
        return $query->result();
    }
}