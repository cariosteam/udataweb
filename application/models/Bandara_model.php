<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bandara_model extends CI_Model {

    var $table = 'airport_tbl';

    public function __construct()
    {
        parent::__construct();

    }
    function index(){
        $this->db->order_by('airport_name','ASC');
        $query=$this->db->select('*')->from($this->table)->get();
        return $query->result();
    }
    function tujuan($id){
        $this->db->order_by('airport_name','ASC');
        $query=$this->db->select('*')->from($this->table)->where('airport_id!=',$id)->get();
        return $query->result();
    }
    function getBandara($id){
        $query=$this->db->select('*')->from($this->table)->where('airport_id',$id)->get();
        return $query->result();
    }

    function updatebandara($id){
        $data=array(
            'airport_name'=>$this->input->post('nama_bandara'),
            'airport_location'=>$this->input->post('lokasi_bandara'),
        );
        $this->db->where('airport_id',$id);
        $this->db->update($this->table,$data);
    }
    function deleted($id){
        $this->db->where('airport_id',$id);
        $this->db->delete($this->table);
    }
    function addbandara(){
        $kode_bandara = $this->input->post('kode_bandara');
        $nama_bandara = $this->input->post('nama_bandara');
        $lokasi_bandara = $this->input->post('lokasi_bandara');
        $data = array(
            'airport_id'=>$kode_bandara,
            'airport_name'=>$nama_bandara,
            'airport_location' =>$lokasi_bandara,);

        $this->db->insert($this->table,$data);
    }
    function get_arrival($id){
        $query=$this->db->select('*')->from($this->table)->where_not_in('airport_id',$id)->get();
        return $query->result();
    }


}

