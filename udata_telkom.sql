-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Agu 2016 pada 21.00
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `udata_telkom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_faq_produk`
--

CREATE TABLE `x_faq_produk` (
  `FAQ_ID` bigint(20) NOT NULL,
  `JUDUL_FAQ` varchar(200) NOT NULL,
  `KONTEN` text NOT NULL,
  `PRODUK` bigint(20) NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `UPDATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_fitur_produk`
--

CREATE TABLE `x_fitur_produk` (
  `FITUR_ID` bigint(20) NOT NULL,
  `ICON` varchar(200) NOT NULL,
  `NAMA_FITUR` varchar(200) NOT NULL,
  `KONTEN` text NOT NULL,
  `PRODUK` bigint(20) NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `UPDATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_klien`
--

CREATE TABLE `x_klien` (
  `KLIEN_ID` bigint(20) NOT NULL,
  `CAT` bigint(20) NOT NULL,
  `NAMA_KLIEN` varchar(200) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `PICTURES` varchar(300) NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `UPDATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_konten`
--

CREATE TABLE `x_konten` (
  `KONTEN_ID` bigint(20) NOT NULL,
  `CAT` bigint(20) NOT NULL,
  `NAMA_KONTEN` varchar(200) NOT NULL,
  `ISI_KONTENT` text NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `UPDATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_news`
--

CREATE TABLE `x_news` (
  `NEWS_ID` bigint(20) NOT NULL,
  `CAT` int(11) DEFAULT NULL,
  `ALIAS` varchar(300) NOT NULL,
  `NEWS_TITLE` varchar(500) DEFAULT NULL,
  `SUB_TITLE` varchar(300) NOT NULL,
  `NEWS_CONTENT` longtext,
  `NEWS_PICTURE` varchar(500) DEFAULT NULL,
  `NEWS_PICTURE_CAPTION` varchar(300) DEFAULT NULL,
  `NEWS_PICTURE_SOURCE` varchar(200) DEFAULT NULL,
  `TAGS` varchar(300) DEFAULT NULL,
  `META_TITLE` varchar(300) DEFAULT NULL,
  `META_KEY` varchar(200) DEFAULT NULL,
  `META_DESC` text,
  `UPDATED_BY` varchar(200) DEFAULT NULL,
  `UPDATED_DATE` datetime DEFAULT NULL,
  `CREATED_BY` varchar(200) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `x_news`
--

INSERT INTO `x_news` (`NEWS_ID`, `CAT`, `ALIAS`, `NEWS_TITLE`, `SUB_TITLE`, `NEWS_CONTENT`, `NEWS_PICTURE`, `NEWS_PICTURE_CAPTION`, `NEWS_PICTURE_SOURCE`, `TAGS`, `META_TITLE`, `META_KEY`, `META_DESC`, `UPDATED_BY`, `UPDATED_DATE`, `CREATED_BY`, `CREATED_DATE`) VALUES
(3, 1, 'peran-data', 'PERAN BIG DATA DALAM BISNIS ADVERTISING KE DEPAN', '', '<p>Jaringan penyiaran yang &ldquo;big&rdquo; pada segmen advertising, belakangan ini menunjukkan tanda-tanda adanya kompleksitas pada dunia media. Inovasi teknologi mendorong media secara fragmentasi dengan membuat masalah baru bagi media TV, radio hingga media cetak. Advertiser sekarang harus membagi anggaran mereka pada &ldquo;perkembangbiakan&rdquo; perangkat dan saluran audiens. Bagaimana advertiser dapat berharap untuk mendapatkan pengembalian atas iklan yang dapat meningkatkan penjualan mereka? Dan bagaimana media dapat menawarkan atas pengembalian seperti yang diharapkan advertiser?</p>\r\n\r\n<p>Pastinya, revolusi teknologi ini akan memperumit hidup kita. Tapi kita harus yakin bahwa revolusi teknologi ini akan sebanding untuk kebangkitan iklan di masa depan.</p>\r\n\r\n<p>Mengapa? Inovasi teknologi memungkinkan untuk mengurangi limbah iklan dan membuatnya tepat sasaran. Apabila iklan akan tepat sasaran, maka akan menjadi lebih lebih efisien sehingga akan menaikkan ROI-nya. ROI yang lebih tinggi ini kemudian akan menyebabkan investasi yang lebih besar kepada iklan.</p>\r\n\r\n<p>Rahasianya adalah tidak hanya data yang &ldquo;besar&rdquo;, melainkan data yang &ldquo;benar&rdquo;.</p>\r\n\r\n<p>Apa yang kita sebut sebagai &ldquo;enterprise marketing platforms&rdquo; akan mengambil data dengan melihat dan membeli jutaan perilaku. Platform tersebut akan menggabungkan unsur model pemasaran, model atribusi dan alat analisis lainnya. Hasilnya berupa prediksi hubungan melihat iklan yang lebih tepat sesuai apa yang akan dibeli sebelumnya.</p>\r\n\r\n<p>Model ini kemudian akan menginformasikan sistem yang sistematis dan pemograman penawaran untuk membeli dan iklan, serta memonitor aktivitas pembelian yang dihasilkan. Hasil ini akan dimasukkan kembali ke dalam model prediksi secara real-time. Model akan menjadi lebih baik dengan data pada siklus sebelumnya, mengurangi limbah iklan. ROI naik, dan pendapatan bertambah.</p>\r\n\r\n<p>Apakah cukup dengan itu? Tidak, itu semua belum cukup. Jika platform pemasaran perusahaan harus melakukan semua hal ini berhasil, kita membutuhkan teknologi yang tepat, analisis yang tepat, dan data yang benar. kini, teknologi ini mendekati pada status komoditas. Analytics itu sulit tapi bisa dilakukan. Tantangan yang sebenarnya adalah data.</p>\r\n\r\n<p>Pertama, privasi konsumen dan keamanan customer harus tetap menjadi prioritas. Data personal customer sangat penting untuk dijaga anonimitas data tanpa kehilangan kekhususan. Meskipun apa yang orang lain katakan, privasi dan presisi bukan zero-sum game, tetapi mencapai kedua membutuhkan perawatan yang sangat besar.</p>\r\n\r\n<p>Kemudian, tantangan manajemen-identitas bahwa tidak semua data cukup akurat. Iklan yang yang terlihat pada smartphone dan tablet: apakah itu terlihat sekali oleh dua orang atau dua kali oleh satu orang?. Beberapa data tidak tersedia cukup cepat. Dan data yang besar biasanya &ldquo;kotor&rdquo; dan tidak representatif.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Hal ini karena data yang besar umumnya merupakan hal yang terakumulasi, bukan sesuatu yang dibangun. Sehingga memerlukan kalibrasi dengan data yang bersih dan representatif.</p>\r\n\r\n<p>Kita sedang memasuki sebuah dunia di mana tidak ada perusahaan akan memiliki cukup data sendiri untuk melakukan apa yang akan dilakukan. Mengumpulkan data yang benar-benar komprehensif akan melibatkan negosiasi dengan pemilik berbagai data dimana hanya dengan orang-orang yang mereka percaya untuk tetap aman dan terpercaya.</p>\r\n\r\n<p>Oleh karena itu, kemudian muncullah perbauran real-time data, teknologi dan analisis yang berhubungan media exposure dari semua konsumen pada setiap perangkat untuk aktivitas pembelian costumer di setiap saluran. Hasilnya akan memungkinkan produsen untuk mengidentifikasi dengan presisi, iklan apa yang paling berkontribusi pada prospek pertumbuhan produk mereka.</p>\r\n\r\n<p>Enabler kepresisian data berupa data yang tepat, dalam bentuk yang tepat, dan ditangani dengan cara yang benar. Inilah yang akan mendorong kebangkitan dunia advertising.</p>\r\n', 'Hoho.jpg', 'peran big data kedepan', 'http://sites.nielsen.com/newscenter/', 'hoho', 'PERAN BIG DATA DALAM BISNIS ADVERTISING KE DEPAN', 'hoho', 'hoho', NULL, NULL, NULL, '2016-08-11 14:56:25'),
(4, 1, 'alasan-berkarir-di-big-data', 'ALASAN BERKARIR DI BIG DATA SANGAT MENARIK', 'Big Data Analytics adalah salah satu keterampilan yang sangat populer saat ini.', '<p>Big Data Analytics adalah salah satu keterampilan yang sangat populer saat ini. Berikut ini adalah alasan bagaimana kita memahami gambaran besar dan meyakinkan untuk menjelajahi bidang menarik ini:</p>\n\n<ol>\n	<li><strong>Komoditas paling bernilai dari perusahaan adalah Data</strong></li>\n</ol>\n\n<p>IDC memproyeksikan bahwa 40 zettabytes (ZB) data akan dihasilkan pada tahun 2020. Setiap sektor dan industri sekarang mengakui potensi dari kedua data baik data yang tidak terstruktur maupun terstruktur. IDC memproyeksikan bahwa pada tahun 2015 organisasi meningkatkan pengeluaran mereka pada Big Data hingga $ 16,9 bio.</p>\n\n<p>Untuk mayoritas organisasi, Big Data Analytics akan muncul sebagai praktik pada bisnis inti. Hal Ini digunakan untuk memahami pelanggan dan bisnis mereka dengan cara baru yang fundamental. Tanpa bantuan dari wawasan yang didukung oleh data, diperkirakan bahwa perusahaan akan mulai kehilangan &ldquo;ketajaman&rdquo;. Dengan data yang terus tumbuh pada rate yang besar dan mengambil nilai tersebut dalam organisasi, maka diperkirakan bahwa segera akan ada beberapa jenis analisis data yang terlibat dalam setiap fungsi. Hal ini berarti bahwa bagi mereka yang memiliki keterampilan analisis data, pertumbuhan mereka akan sangat tinggi.</p>\n\n<ol>\n	<li><strong>Ratusan permintaan tenaga Data Science tidak dapat dipenuhi.</strong></li>\n</ol>\n\n<p>Kesenjangan pasokan dan permintaan talent analytics lebih besar daripada persediaan. Terdapat kekurangan 50 sampai 60% di tahun 2018. Dibutuhkan lebih dari 1,5 juta data analis dan manajer dengan kemampuan memahami dan membuat keputusan berdasarkan big data. Ketika pekerjaan Anda dalam permintaan, Anda akan dibayar dengan gaji yang lebih tinggi, yang ditawarkan melebihi tunjangan, dan memiliki jalur karir yang luar biasa ke depannya.</p>\n\n<ol>\n	<li><strong>Potensi besar dalam pergerakan karir</strong></li>\n</ol>\n\n<p>Dalam menghadapi kekurangan dalam talent analytics, Anda dapat bekerja di semua jenis industri. Hal ini memungkinkan Anda untuk bergerak menaiki karir secepat Anda terus mendapatkan lebih banyak pengalaman. Hal ini membuat Anda menjadi lebih mudah untuk bergerak lebih cepat ke posisi strategis.</p>\n\n<ol>\n	<li><strong>Menyediakan jenis pekerjaan yang selalu menantang</strong></li>\n</ol>\n\n<p>Dalam sebuah studi yang dilakukan oleh Glassdoor tentang keseimbangan kehidupan kerja, Data Scientist berada pada peringkat tertinggi. Pada skala lima poin, mencapai poin 4.4. Bekerja sebagai Data Scientist yang melibatkan Anda menjadi bagian artist dan bagian analis. Setiap proyek itu unik. Anda akan melihat semua jenis data dan menemukan cara unik untuk memberikan wawasan dan menyajikannya dalam cara visual yang menarik. Wawasan yang ditawarkan akan membawa perubahan nyata untuk organisasi Anda dalam bekerja.</p>\n', NULL, 'steve jobs', 'http://sites.nielsen.com/newscenter/', 'hahah', 'ALASAN BERKARIR DI BIG DATA SANGAT MENARIK', 'hihi', 'huhu', NULL, NULL, NULL, '2016-08-11 16:16:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_news_category`
--

CREATE TABLE `x_news_category` (
  `ID` int(11) NOT NULL,
  `CAT_ALIAS` varchar(500) DEFAULT '',
  `CAT_NAME` varchar(200) DEFAULT '',
  `COLOR` varchar(10) DEFAULT '#ff7903',
  `PARENT` int(11) DEFAULT NULL,
  `CAT_ORDER` tinyint(4) DEFAULT '0',
  `META_DESC` text,
  `META_KEYWORD` varchar(200) DEFAULT NULL,
  `UPDATED_BY` varchar(100) DEFAULT NULL,
  `UPDATED_DATE` datetime DEFAULT NULL,
  `CREATED_BY` varchar(100) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  `TYPE` smallint(1) DEFAULT '1' COMMENT '1 = berita, 2 = artikel'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `x_news_category`
--

INSERT INTO `x_news_category` (`ID`, `CAT_ALIAS`, `CAT_NAME`, `COLOR`, `PARENT`, `CAT_ORDER`, `META_DESC`, `META_KEYWORD`, `UPDATED_BY`, `UPDATED_DATE`, `CREATED_BY`, `CREATED_DATE`, `TYPE`) VALUES
(23, 'berita-umum', 'Berita Umum', '0', NULL, 0, '', 'berita, umum', 'jametson', '2015-10-20 13:51:19', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_news_comment`
--

CREATE TABLE `x_news_comment` (
  `ID` bigint(20) NOT NULL,
  `USER` int(11) DEFAULT NULL,
  `NEWS` bigint(20) DEFAULT NULL,
  `COMMENT` text,
  `STATUS` varchar(5) DEFAULT 'P' COMMENT 'A = Approve, P = Pending, R = Reject, '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_news_tags`
--

CREATE TABLE `x_news_tags` (
  `ID` bigint(20) NOT NULL,
  `TAG` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_pages`
--

CREATE TABLE `x_pages` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(300) DEFAULT NULL,
  `ALIAS` varchar(500) DEFAULT NULL,
  `CONTENT` text,
  `ORDER` tinyint(4) DEFAULT '0',
  `META_KEY` varchar(200) DEFAULT NULL,
  `META_DESC` text,
  `STATUS` char(1) DEFAULT 'A' COMMENT 'A = Aktif, N = Tidak Aktif',
  `PARENT` int(11) DEFAULT NULL,
  `PAGE_VIEW` int(11) DEFAULT '0',
  `COLOR` varchar(10) DEFAULT NULL,
  `CREATED_BY` varchar(200) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  `UPDATED_BY` varchar(200) DEFAULT NULL,
  `UPDATED_DATE` datetime DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `REF_URL` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `x_pages`
--

INSERT INTO `x_pages` (`ID`, `TITLE`, `ALIAS`, `CONTENT`, `ORDER`, `META_KEY`, `META_DESC`, `STATUS`, `PARENT`, `PAGE_VIEW`, `COLOR`, `CREATED_BY`, `CREATED_DATE`, `UPDATED_BY`, `UPDATED_DATE`, `TYPE`, `REF_URL`) VALUES
(25, 'Home', 'home', '', 1, '', '', 'A', NULL, 1, '#4ccc8f', 'efriandika', '2014-06-08 21:59:57', 'efriandika', '2014-09-22 15:26:11', 2, ''),
(26, 'Berita', 'berita', '', 5, '', '', 'A', NULL, 1, '#db549d', 'efriandika', '2014-06-08 22:03:40', 'jametson', '2015-10-20 13:46:21', 2, 'berita'),
(28, 'Galeri', 'galeri', '', 6, '', '', 'A', NULL, 0, '#fcf9f9', 'efriandika', '2014-06-08 22:05:07', 'jametson', '2015-08-18 01:17:43', 2, 'foto'),
(32, 'Profil Biro', 'profil-biro', '', 2, 'dikmental, jakarta', '', 'A', NULL, 0, '#ff0000', 'jametson', '2015-08-18 01:05:44', 'jametson', '2015-10-20 13:45:39', 2, '#'),
(33, 'Visi Misi dan Tugas Pokok', 'visi-misi-dan-tugas-pokok', '<p><strong>Visi </strong></p>\n\n<p>Terciptanya koordinasi program lintas sector secara optimal dalam rangka menjadikan Biro Pendidikan dan Mental Spiritual Setda Provinsi DKI Jakarta sebagai <em>&ldquo;Center of Supporting&rdquo; </em>(Pusat Pendukung) program pada Bidang Kesejahteraan Masyarakat Provinsi DKI Jakarta.</p>\n\n<p><strong>Misi</strong></p>\n\n<p>Terwujudnya Koordinasi yang memiliki pronsip efektif, transparan, partisipasif, berkelanjutan, dan akuntabilitas di bidang Pendidikan, Perpustakaan, dan Arsip Daerah, Olahraga dan Pemuda dan Mental Spiritual di Provinsi DKI Jakarta</p>\n\n<p><strong>Tugas Pokok</strong></p>\n\n<p>Sebagaimana tercantum pada pasal 203 Peraturan Gubernur Provinsi DKI Jakarta Nomor 247 Tahun 2014 &nbsp;tentang Otganisasi dan Tata Kerja Sekda, Biro Pendidikan dan Mental Spiritual Setda Provinsi DKI Jakarta mempunyai tugas pokok :</p>\n\n<ul>\n	<li>Melaksanakan Penyusunan Kebijakan</li>\n	<li>Mengkoordinasikan</li>\n	<li>Memantau</li>\n	<li>Mengevaluasi dan</li>\n	<li>Membina Administrasi</li>\n</ul>\n', 2, 'visi, misi', 'Visi Misi dan Tugas Pokok', 'A', 32, 0, '#f7f4f4', 'jametson', '2015-08-18 01:08:35', 'jametson', '2015-10-20 13:28:21', 1, ''),
(34, 'Bagian', 'bagian', '', 3, 'bagian, dimental', 'Bagian Dimental', 'A', NULL, 0, '#faefef', 'jametson', '2015-08-18 01:11:02', 'jametson', '2015-10-20 13:46:54', 2, '#'),
(35, 'Bagian Pendidikan dan Perpustakaan', 'bagian-pendidikan-dan-perpustakaan', '<p>Bagian Pendidikan dan Perpustakaan &nbsp;merupakan Unit kerja Biro Pendidikan dan Mental Spiritual dalam pelaksanaan perumusan, pengoordinasian, pembinaan, pemantauan dan evaluasi pelaksanaan kebijakan pemerintah daerah mengenai pendidikan, perpustakaan daerah dan arsip daerah</p>\n\n<p>Fungsi Bagian Pendidikan, Perrpustakaan dan Arsip :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>penyusunan rencana strategis dan rencana kerja dan anggaran Bagian Pendidikan dan Perpustakaan;</li>\n	<li>pelaksanaan rencana strategis dan dokumen pelaksanaan anggaran Bagian Pendidikan dan Perpustakaan;</li>\n	<li>penghimpunan dan pengolahan data dan informasi sebagai bahan penyusunan kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal, perpustakaan dan kearsipan daerah;</li>\n	<li>perumusan, penyusunan dan pembahasan kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal perpustakaan dan kearsipan daerah;</li>\n	<li>pelaksanaan sosialisasi kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal perpustakaan dan kearsipan daerah;</li>\n	<li>pengoordinasian pelaksanaan kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal perpustakaan dan kearsipan daerah;</li>\n	<li>pemantauan dan pembinaan pelaksanaan kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal, perpustakaan dan kearsipan daerah;</li>\n	<li>pengevaluasian pelaksanaan kebijakan pendidikan dasar, pendidikan menengah, pendidikan nonformal dan informal, perpustakaan dan kearsipan daerah;</li>\n	<li>penyiapan bahan laporan Biro Pendidikan dan Mental Spiritual yang terkait dengan tugas dan fungsi Bagian Pendidikan dan Perpustakaan;</li>\n	<li>pelaporan dan pertanggungjawaban pelaksanaan tugas dan fungsi Bagian Pendidikan dan Perpustakaan</li>\n</ol>\n\n<p style="margin-left:21.3pt">Bagian Pendidikan, Perrpustakaan dan Arsip terdiri dari :</p>\n\n<ol>\n	<li>Sub Bagian Pendidikan Dasar dan Menengah</li>\n	<li>Sub Bagian Pendidikan Non Formal dan Informal</li>\n	<li>Sub Bagian Perpustakaan dan Arsip</li>\n</ol>\n', 1, 'Bagian, Pendidikan,Perpustakaan', 'Bagian Pendidikan dan Perpustakaan', 'A', 34, 0, '#f5f4f4', 'jametson', '2015-08-18 01:12:06', 'jametson', '2015-10-20 13:33:53', 1, ''),
(36, 'Bagian Mental Spiritual', 'bagian-mental-spiritual', '<p style="margin-left:21.3pt"><strong>Bagian Mental Spiritual</strong></p>\n\n<p>Bagian Mental Spiritual merupakan Unit Kerja Biro Pendidikan dan Mental Spiritual dalam pelaksanaan pemmusan, pengoordinasian, pembinaan, pemantauan dan evaluasi pelaksanaan kebijakan pemerintah daerah di bidang mental spiritual masyarakat, lembaga mental spiritual dan fasilitas keagamaan</p>\n\n<p>Fungsi Bagian Mental Spiritual</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>penyusunan rencana strategis dan rencana kerja dan anggaran Bagian Mental Spiritual;</li>\n	<li>pelaksanaan rencana strategis dan dokumen pelaksanaan anggaran Bagian Mental Spiritual;</li>\n	<li>penghimpunan dan pengolahan data dan informasi sebagai bahan&nbsp;&nbsp; penyusunan&nbsp;&nbsp; kebijakan fasilitasi,&nbsp;&nbsp; pengoordinasian&nbsp;&nbsp; dan &nbsp;&nbsp;pengembangan kegiatan kemasyarakatan dan mental spiritual;</li>\n	<li>pemrosesan pelayanan perizinan pendirian rumah ibadah;</li>\n	<li>pengkoordinasian pelaksanaan belanja h bah dan/atau bantuan sosial kepada lembaga mental spiritual;</li>\n	<li>pelaksanaan fasilitasi penyelenggaraan kegiatan kemasyarakatan dan mental spiritual;</li>\n	<li>pelaksanaan fasilitasi penyelenggaraan kegiatan lembaga keagamaan;</li>\n	<li>&nbsp;pelaksanaan fasilitasi penyelenggaraan ibadah haji;</li>\n	<li>pelaksanaan penyeleksian, pelatihan, pemberangkatan, pemulangan dan pelaporan petugas haji daerah;</li>\n	<li>pengoordinasian penyelenggaraan haji di daerah;</li>\n	<li>pengoordinasian, pemantauan, pembihaan dan pengevaluasian pelaksanaan kebijakan fasilitasi, pengoordinasian dan pengembangan kegiatan kemasyarakatan dan mental spiritual;</li>\n	<li>pelaksanaan sosialisasi kebijakan kegiatan kemasyarakatan dan mental spiritual;</li>\n	<li>penyusunan bahan pemberian saran dan pertimbangan kepada pimpinan yang berkaitan dengan keagamaan;</li>\n	<li>pelaporan dan pertanggungjawaban pelaksanaan tugas dan fungsi Bagian Mental Spiritual;</li>\n</ol>\n\n<p style="margin-left:21.3pt">Bagian Pendidikan, Perrpustakaan dan Arsip terdiri dari :</p>\n\n<ol>\n	<li>Sub Bagian Pendidikan Fasilitas Kegiatan Masyarakat</li>\n	<li>Sub Bagian Lembaga Mental Spiritual</li>\n	<li>Sub Bagian Fasilitas Kegiatan Mental Spiritual</li>\n</ol>\n', 2, 'Bagian,Mental,Spiritual', 'Bagian Mental Spiritual', 'A', 34, 0, '#f5efef', 'jametson', '2015-08-18 01:13:36', 'jametson', '2015-10-20 13:34:43', 1, ''),
(37, 'Bagian Olahraga dan Pemuda', 'bagian-olahraga-dan-pemuda', '<p><strong>Bagian Olahraga dan Pemuda</strong></p>\n\n<p>Bagian Olahraga dan Pemuda merupakan Unit Kerja Biro Pendidikan dan Mental Spiritual dalam pelaksanaan perumusan, Kebijakan, serta pengoordinasian, pembinaan, pemantauan dan evaluasi pelaksanaan kebijakan pemerintah daerah mengenai Olahraga dan Pemuda.</p>\n\n<p>Fungsi Bagian Olahraga dan Pemuda :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>penyusunan rencana strategis dan rencana kerja dan anggaran Bagian Olahraga dan Pemuda sesuai dengan lingkup tugasnya;</li>\n	<li>pelaksanaan rencana strategis dan dokume pelaksanaan anggaran Bagian Olahraga dan Pemuda sesuai dengan lingkup tugasnya;</li>\n	<li>pelaksanaan kegiatan dan anggaran Biro Pendidikan dan Mental Spiritual sesuai dengan lingkup tugas dan fungsi Bagian Olahraga dan Pemuda;</li>\n	<li>penghimpunan dan pengolahan data dan informasi sebagai bahan penyusunan kebijakan olahraga dan pemuda;</li>\n	<li>perumusan, penyusunan dan pembahasan kebijakan olahraga dan pemuda;</li>\n	<li>pelaksanaan sosialisasi kebijakan olahraga dan pemuda;</li>\n	<li>pengoordinasian pelaksanaan kebijakan olahraga dan pemuda;</li>\n	<li>pemantauan dan pembinaan pelaksanaan kebijakan olahraga dan pemuda</li>\n	<li>pengevaluasian pelaksanaan kebijakan olahraga dan pemuda.</li>\n	<li>penyiapan fasilitasi koordinasi pelaksanaan tugas dan fungsi Dinas dan Badan Bidang Pendidikan dan Mental Spiritual;</li>\n	<li>penyusunan rencana strategis Biro Pendidikan dan Mental Spiritual;</li>\n	<li>pengelolaan kepegawaian, keuangan, barang dan ketatausahaan Biro Pendidikan dan Mental Spiritual;</li>\n	<li>pengoordinasian penyusunan rencana strategis dan rencana kerja dan anggaran Biro Pendidikan dar. Mental Spiritual;</li>\n	<li>&nbsp;pengoordinasian penyusunan laporan keuangan, kegiatan. kinerja dan akuntabilitas Biro Pendidikan dan Mental Spiritual; dan</li>\n	<li>&nbsp;pelaporan dan pertanggungjawaban pelaksanaan tugas dan fungsi Bagian Olahraga dan Pemuda.</li>\n</ol>\n\n<p style="margin-left:21.3pt">&nbsp;</p>\n\n<p style="margin-left:21.3pt">Bagian Olahraga dan Pemuda terdiri dari :</p>\n\n<ol>\n	<li>Sub Bagian Pemuda</li>\n	<li>Sub Bagian Olahraga</li>\n	<li>Sub Bagian Tata Usaha</li>\n</ol>\n', 3, 'Bagian,Olahraga,Pemuda', 'Bagian Olahraga dan Pemuda', 'A', 34, 0, '#fff9f9', 'jametson', '2015-08-18 01:14:57', 'jametson', '2015-10-20 13:35:58', 1, ''),
(38, 'Lembaga Koordinasi', 'lembaga-koordinasi', '', 4, '', '', 'A', NULL, 0, '#faf9f9', 'jametson', '2015-08-18 01:17:07', 'jametson', '2015-10-20 13:47:09', 2, '#'),
(39, 'Kontak Kami', 'kontak-kami', '', 7, '', '', 'A', NULL, 0, '#faf6f6', 'jametson', '2015-08-18 01:18:17', NULL, NULL, 2, 'kontak'),
(40, 'Produk Hukum', 'produk-hukum', '', 6, '', '', 'A', NULL, 0, '#fcf6f6', 'jametson', '2015-08-18 01:18:46', 'jametson', '2015-10-20 13:44:30', 2, 'e_library'),
(45, 'BAZIS', 'bazis', '<p style="margin-left:21.3pt"><strong>BAZIS (Badan Amil Zakat, Infaq dan Shadaqah Provinsi DKI Jakarta ) </strong>adalah lembaga non struktural Pemerintah Daerah di bidang pengumpulan dan pendayagunaan Zakat, Infaq dan Shadaqah. Bazis merupakan institusi publik yang terdiri dari unsur Pemerintah Daerah dan Masyarakat yang dipimpin oleh seorang Kepala yang berada di bawah dan bertanggung jawab kepada Gubernur. Dalam melaksanakan tugas dan fungsinya, BAZIS dikoordinasikan oleh Asisten Kesejahteraan Rakyat. Tugas BAZIS yaitu menyelenggarakan pengumpulan dan pendayagunaan zakat, infaq dan shadaqah sesuai dengan fungsi dan tujuaanya</p>\n\n<p style="margin-left:21.3pt">&nbsp;</p>\n\n<p style="margin-left:21.3pt">Fungsi BAZIS adalah</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Penyusunan program</li>\n	<li>Pengumpulan segala macam zakat, infaq dan shadaqah dari masyarakat termasuk pegawai di wilayah Provinsi DKI Jakarta</li>\n	<li>Pendayagunaan zakat, infaq dan shadaqah sesuai dengan ketentuan hukumnya</li>\n	<li>Penyuluhan kepada masyarakat dalam upaya peningkatan kesadaran menunaikan ibadah zakat, infaq dan shadaqah</li>\n	<li>Pembinaan pemanfaatan zakat, infaq dan shadaqah agar lebih produktif dan terarah</li>\n	<li>Koordinasi, bimbingan dan pengawasan kegiatan pengumpulan zakat, infaq dan shadaqah yang dilaksanakan oleh pelaksana pengumpulan BAZIS</li>\n	<li>Penyelenggaraan kerja sama dengan Badan Amil Zakat, Infaq dan Shadaqah dan lembaga amil zakat yang lain.</li>\n	<li>Pengendalian atas pelaksanaan pengumpulan dan pendayagunaan zakat, infaq dan shadaqah</li>\n	<li>Pengurusan fungsi-fungsi ketatausahaan, perlengkapan, kerumahtanggaan dan sumberdaya manusia.</li>\n</ol>\n\n<p style="margin-left:21.3pt"><strong>Susunan Organisasi</strong></p>\n\n<p style="margin-left:21.3pt">Organisasi BAZIS terdiri dari Dewan Pertimbangan, Komisi Pengawas dan Badan Pelaksana</p>\n\n<p style="margin-left:21.3pt">Susunan Organisasi Badan Pelaksana BAZIS terdiri dari :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Kepala</li>\n	<li>Wakil Kepala</li>\n	<li>Sekretariat</li>\n	<li>Bidang Pengumpulan</li>\n	<li>Bidang Pendayagunaan</li>\n	<li>Bidang Dana</li>\n	<li>Pelaksana BAZIS Kotamadya/Kabupaten Administrasi</li>\n</ol>\n', 1, 'bazis, amil, zakat', 'Badan Amil Zakat, Infaq dan Shadaqah (BAZIS) Provinsi DKI Jakarta', 'A', 38, 0, '#ff0000', 'jametson', '2015-10-20 13:38:54', NULL, NULL, 1, ''),
(46, 'LTPQ', 'ltpq', '<p style="margin-left:21.25pt"><strong>Lembaga Pengembangan Tilawatil Qur&rsquo;an (LPTQ) Provinsi DKI Jakarta </strong>merupakan hasil dari upaya pelembagaan MTQ secara Nasional yang telah melalui berbagai proses yang cukup panjang. Perjuangan ke arah terwujudnya pelembagaan secara Nasional tersebut dipelopori oleh Lembaga Pembina (LP) MTQ DKI Jakarta dengan berbagai cara dan melalui berbagai forum.</p>\n\n<p style="margin-left:21.25pt">Bertitik tolak dari konsep dasar itulah, maka terwujudlah pelembagaan MTQ Nasional dengan nama Lembaga Pengembangan Tilawatil Qur&rsquo;an (LPTQ) melalui Surat Keputusan Bersama (SKB) Menteri Agama RI Nomor 19 Tahun 1977 dan Menteri Dalam Negeri RI Nomor 151 Tahun 1977.</p>\n\n<p style="margin-left:21.25pt">Dengan keluarnya SKB tersebut, maka pada tahun 1978 LPTQ Provinsi DKI Jakarta dibentuk melalui Surat Keputusan Gubernur Kepala Daerah Khusus Ibukota Jakarta Nomor 98 Tahun 1978.</p>\n\n<p style="margin-left:21.25pt">Sejak berdirinya LPTQ Provinsi DKI Jakarta hingga saat ini telah beberapa kali berhasil membawa nama harum Provinsi DKI Jakarta pada even STQ / MTQ baik di tingkat Nasional maupun Internasional, yang mana semua itu tak lepas dari usaha dan perjuangan yang keras baik dari Pengurus, Pembina, Pelatih maupun para Peserta.</p>\n\n<p style="margin-left:21.25pt">LPTQ merupakan lembaga non perangkat daerah di bidang keagamaan yang mengkoordinasikan pengembangan Tilawatil Qur&rsquo;an di Daerah yang dipimpin oleh Seorang Ketua Umum yang berada di bawah tanggungjawab Gubernur melalui Dewan Pembina.</p>\n\n<p style="margin-left:21.25pt">LPTQ bertujuan untuk mewujudkan penghayatan dan pengamalan Al-Qur&rsquo;an pada masyarakat Indonesia yang ber Pancasila</p>\n\n<p style="margin-left:21.3pt"><strong>Tugas LPTQ</strong> :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Menyelenggarakan Musabaqah Tilawatil Qur&rsquo;an di Tingkat Daerah</li>\n	<li>Menyelenggarakan Pembinaan Tilawah (baca dan lagu), Tahfidz (hafalan), Khat (tulia indah), Puitisasi (Isi kandungan Al-Qur&rsquo;an) dan Pameran Al-Qur&rsquo;an.</li>\n	<li>Meningatkan pemahaman Al-Qur&rsquo;an melalui penerjemahan, penafsiran, pengkajian dan klasifikasi ayat-ayat.</li>\n	<li>Meningkatkan penghayatan dan pengamalan Al-Qur&rsquo;an dalam kehidupan sehari-hari.</li>\n</ol>\n\n<p style="margin-left:21.3pt"><strong>LPTQ menjalankan fungsi</strong> :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Mengadakan penjabaran program umum yang ditetapkan pada Musyawarah Nasional dan melaksanakan evaluasi pada Rapat Kerja Daerah dalam ruang lingkup Tingkat Daerah.</li>\n	<li>Menyelenggarakan kegiatan-kegiatan dan pengambilan keputusan yang dianggap perlu dalam rangka pencapaian tujuan, tugas pokok dan program LPTQ.</li>\n	<li>Menyelenggarakan pembinaan, bimbingan dan koordinasi terhadap Pengembangan Tilawatil Qur&rsquo;an di Daerah</li>\n</ol>\n\n<p style="margin-left:21.3pt"><strong>Susunan Pengurus LPTQ terdiri dari :</strong></p>\n\n<ol style="list-style-type:upper-roman">\n	<li>Dewan Pembina terdiri dari :</li>\n</ol>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Ketua</li>\n	<li>Ketua Harian</li>\n	<li>Sekretaris</li>\n	<li>Anggota</li>\n</ol>\n\n<p style="margin-left:42.55pt">&nbsp;</p>\n\n<ol style="list-style-type:upper-roman">\n	<li>Pengurus terdiri dari :</li>\n</ol>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Ketua Umum</li>\n	<li>Ketua I</li>\n	<li>Ketua II</li>\n	<li>Ketua III</li>\n	<li>Sekretaris Umum</li>\n	<li>Sekretaris I</li>\n	<li>Sekretais II</li>\n	<li>Sekretaris III</li>\n	<li>Bendahara I</li>\n	<li>Bendahara II</li>\n</ol>\n\n<p>Para Ketua dan Anggota Bidang</p>\n', 2, 'Lembaga,Pengembangan,Tilawatil,Qur’an,LPTQ', 'Lembaga Pengembangan Tilawatil Qur’an (LPTQ) Provinsi DKI Jakarta', 'A', 38, 0, '#ff0000', 'jametson', '2015-10-20 13:40:44', NULL, NULL, 1, ''),
(47, 'LBIQ', 'lbiq', '<p><strong>Lembaga Bahasa dan Ilmu Al-Qur&rsquo;an (LBIQ) </strong> Provinsi DKI Jakarta dibentuk melalui Surat Keputusan Gubernur Kepala Daerah Provinsi DKI Jakarta Nomor 2745 tahun 1984 yang disempurnakan dengan Keputusan Gubernur DKI Jakarta Nomor 83 tahun 1986 dan terakhir disempurnakan lagi dengan Peraturan Gubernur Provinsi Daerah Khusus Ibukota Jakarta Nomor 42 tahun 2014 tentang Pembentukan Organisasi dan Tata Kerja Lembaga Bahasa dan Ilmu Al-Qur&rsquo;an Provinsi Daerah Khusus Ibukota Jakarta.</p>\n\n<p style="margin-left:21.3pt">LBIQ adalah lembaga non perangkat daerah di bidang keagamaan yang menyelenggarakan pendidikan dan pengajaran bahasa dan Ilmu Al-Qur&rsquo;an di Provinsi Daerah Khusus Ibukota Jakarta yang &nbsp;dipimpin oleh seorang Kepala Lembaga yang berada di bawah dan bertanggung jawab langsung kepada Gubernur melalui Dewan Pembina. Dalam melaksanakan tugasnya LBIQ di bawah koordinasi Biro Pendidikan dan Mental Spiritual Setda Provinsi DKI Jakarta pada Sekretaris Daerah</p>\n\n<p>&nbsp;</p>\n\n<p style="margin-left:21.3pt">LBIQ mempunyai tugas pokok menyelenggarakan Pendidikan dan Pengajaran Bahasa dan Ilmu Al-Qur&rsquo;an serta penelitian dan pengembangan materi dan metodenya.</p>\n\n<p style="margin-left:21.3pt"><strong>Fungsi LBIQ adalah :</strong></p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>menyusun rencana dan program LBIQ;</li>\n	<li>menyelenggarakan pendidikan dan pengajaran bahasa, membaca dan memahami isi dan makna Al Qur&rsquo;an;</li>\n	<li>menyelenggarakan penelitian dan pengembangan materi dan metode pendidikan dan latihan tenaga pengajar, pengajaran bahasa,<br />\n	membaca dan memahami isi dan makna Al-Qur&#39;an;</li>\n	<li>menyelenggarakan pendidikan dan latihan tenaga pengajar (instruktur) untuk pengajaran bahasa, membaca dan memahami isi dan<br />\n	makna Al-Qur&#39;an;</li>\n	<li>menyelenggarakan kegiatan perpustakaan, dokumentasi dan publikasi;</li>\n	<li>menyelenggarakan hubungan kerja sama dengan instansi/lembaga lain di bidang Bahasa dan llmu Al-Qur&#39;an;</li>\n	<li>melaksanakan ketatausahaan yang meliputi surat menyurat kepegawaian, keuangan dan kerumahtanggaan.</li>\n</ol>\n\n<p style="margin-left:21.3pt">&nbsp;</p>\n\n<p style="margin-left:21.6pt"><strong>Badan Pembina LBIQ :</strong></p>\n\n<ol>\n	<li>Ketua;</li>\n	<li>Wakil Ketua;</li>\n	<li>Sekretaris; dan</li>\n	<li>Anggota.</li>\n</ol>\n\n<p style="margin-left:21.6pt"><strong>Pengurus LBIQ terdiri dari:</strong></p>\n\n<ol>\n	<li>Kepala Lembaga;&nbsp;&nbsp;&nbsp;</li>\n	<li>Subbagian Tata Usaha;</li>\n	<li>Seksi Penelitian dan Pengembangan;</li>\n	<li>Seksi Pendidikan dan La&#39;ihan Tenaga Pengajar;</li>\n	<li>Seksi Pengajaran; dan</li>\n	<li>Seksi Perpustakaan, Dokumentasi dan Publikasi.</li>\n</ol>\n', 3, 'Lembaga,Bahasa,Ilmu,Al-Qur’an', 'Lembaga Bahasa dan Ilmu Al-Qur’an (LBIQ) ', 'A', 38, 0, '#324ea5', 'jametson', '2015-10-20 13:42:44', NULL, NULL, 1, ''),
(48, 'KODI', 'kodi', '<p style="margin-left:21.3pt"><strong>Koordinasi Dakwah Islam (KODI) Provinsi DKI Jakarta</strong> adalah lembaga kerjasama antara Pemerintah Daerah dengan Masyarakat di bidang koordinasi, sinkroniasi dan dinamisasi kegiatan Dakwah yang dipimpin oleh seorang Ketua Umum yang berada di bawah dan bertanggung jawab kepada Gubernur Kepala Daerah</p>\n\n<p style="margin-left:21.3pt">KODI dalam melaksanakan tugasnya berada di bawah koordinasi administratif Sekretariat Wilayah/Daerah.</p>\n\n<p style="margin-left:21.3pt">&nbsp;</p>\n\n<p style="margin-left:21.3pt"><strong>Fungsi KODI :</strong></p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>menyusun dan melaksanakan program kerja KODI;</li>\n	<li>menyelenggarakan pertemuan/rapat koordinasi secara &nbsp;periodik atau pertemuan lainnya yang dlpaandang perlu untuk pembinaan lembaga/organisasi dakwah di Daerah Khusus Ibukota Jakarta;</li>\n	<li>merumuskan kebijaksanaan umum dan pelaksanaan operasional pembinaan lembaga/organisasi dakwah;</li>\n	<li>melaksanakan usaha dinamisasi kegiatan dakwah melalui penataran, diskusi, seminar, mudzakarah, dan penerbitan;</li>\n	<li>mengkoordinasikan rencana dan mengarahkan penggunaan dana dan peralatan dari Pemerintah Daerah&nbsp; serta bantuan lainnya untuk pembinaan lembaga/organisasi&#39; dakwah;</li>\n	<li>melaksanakan monitoring, analisis dan evaluasi penyelenggaraan dakwah di Daerah Khusus Ibukota Jakarta;</li>\n	<li>mengolah dan merumuskan masukan dari masyarakat untuk menjadi bahan penyusunan kebijaksanaan Pemerintah Daerah yang berhubungan dengan dakwah;</li>\n	<li>melaksanakan fungsi-fungsi lain yang berhubungan dengan masalah kegiatan dakwah dan urusan ketatalaksanaan KODI.</li>\n</ol>\n\n<p style="margin-left:21.3pt"><strong>Organisasi KODI Terdiri dari</strong></p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Pengurus</li>\n	<li>Sekretariat</li>\n</ol>\n\n<p style="margin-left:21.3pt"><br />\nPengurus terdiri dari :</p>\n\n<ol style="list-style-type:lower-alpha">\n	<li>Ketua Umum;</li>\n	<li>Ketua I;</li>\n	<li>Ketua II;</li>\n	<li>Ketua III;</li>\n	<li>Anggota</li>\n</ol>\n', 4, 'Koordinasi,Dakwah,Islam,KODI', 'Koordinasi Dakwah Islam (KODI) Provinsi DKI Jakarta', 'A', 38, 0, '#ff0000', 'jametson', '2015-10-20 13:43:55', NULL, NULL, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_poto_gal`
--

CREATE TABLE `x_poto_gal` (
  `ID` int(11) NOT NULL,
  `ID_KAT` int(11) DEFAULT NULL,
  `KANAL` int(11) DEFAULT NULL,
  `TITLE` varchar(200) DEFAULT NULL,
  `POTOGRAFER` varchar(100) DEFAULT NULL,
  `URL` varchar(500) DEFAULT NULL,
  `EMBED` text COMMENT 'I',
  `HTML` text,
  `DESC` text,
  `TYPE` char(1) DEFAULT NULL COMMENT 'I = image, F = Flash, C = Custom',
  `POTO_DATE` varchar(11) NOT NULL,
  `STATUS` char(1) DEFAULT NULL COMMENT 'A = Aktif, N = Non Aktif',
  `CREATED_BY` varchar(100) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  `UPDATED_BY` varchar(100) DEFAULT NULL,
  `UPDATED_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_produk`
--

CREATE TABLE `x_produk` (
  `PRODUK_ID` bigint(20) NOT NULL,
  `NAMA_PRODUK` varchar(200) NOT NULL,
  `CAT` bigint(20) NOT NULL,
  `SUB_BANNER` varchar(300) NOT NULL,
  `MAIN_BANNER` varchar(300) NOT NULL,
  `KONTEN` text NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `UPDATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_testimoni`
--

CREATE TABLE `x_testimoni` (
  `TESTIMONI_ID` bigint(20) NOT NULL,
  `PEOPLES` varchar(40) NOT NULL,
  `TESTIMONI_CONTENT` text NOT NULL,
  `CREATED_AT` datetime NOT NULL,
  `PICTURES` varchar(300) NOT NULL,
  `KLIEN` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `x_users`
--

CREATE TABLE `x_users` (
  `ID` int(11) NOT NULL,
  `USERNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(200) NOT NULL,
  `FULLNAME` varchar(200) DEFAULT NULL,
  `IMAGE` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PHONE` varchar(50) DEFAULT NULL,
  `ADDRESS` varchar(300) DEFAULT NULL,
  `BIO` longtext,
  `STATUS` char(4) DEFAULT NULL,
  `ROLE` tinyint(4) NOT NULL DEFAULT '3',
  `UPDATED_BY` varchar(100) DEFAULT NULL,
  `UPDATED_DATE` datetime DEFAULT NULL,
  `CREATED_BY` varchar(255) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `x_users`
--

INSERT INTO `x_users` (`ID`, `USERNAME`, `PASSWORD`, `FULLNAME`, `IMAGE`, `EMAIL`, `PHONE`, `ADDRESS`, `BIO`, `STATUS`, `ROLE`, `UPDATED_BY`, `UPDATED_DATE`, `CREATED_BY`, `CREATED_DATE`) VALUES
(1, 'jametson', '69a5ad4945060907d0bd682b6aa2136a', 'Admin Web', 'efriandika.png', 'rahan.rama@gmail.com', '08128627250', '<p>Jln. Johar VIII, No. 9<br />\nTaman Cimanggu, Kota Bogor<br />\n<strong>Jawa Barat</strong></p>\n', '', 'A', 1, 'jametson', '2015-08-22 13:05:21', 'admin', '2013-05-07 14:34:32'),
(2, 'dikmentaladmin', '69a5ad4945060907d0bd682b6aa2136a', 'Admin Biro', NULL, 'biro_pms@yahoo.com', '-', '', '\n', 'A', 1, NULL, NULL, 'jametson', '2015-08-22 13:23:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `x_faq_produk`
--
ALTER TABLE `x_faq_produk`
  ADD PRIMARY KEY (`FAQ_ID`),
  ADD KEY `PRODUK` (`PRODUK`);

--
-- Indexes for table `x_fitur_produk`
--
ALTER TABLE `x_fitur_produk`
  ADD PRIMARY KEY (`FITUR_ID`),
  ADD KEY `PRODUK` (`PRODUK`);

--
-- Indexes for table `x_klien`
--
ALTER TABLE `x_klien`
  ADD PRIMARY KEY (`KLIEN_ID`);

--
-- Indexes for table `x_konten`
--
ALTER TABLE `x_konten`
  ADD PRIMARY KEY (`KONTEN_ID`);

--
-- Indexes for table `x_news`
--
ALTER TABLE `x_news`
  ADD PRIMARY KEY (`NEWS_ID`),
  ADD KEY `CAT` (`CAT`);

--
-- Indexes for table `x_news_category`
--
ALTER TABLE `x_news_category`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `PARENT` (`PARENT`);

--
-- Indexes for table `x_news_comment`
--
ALTER TABLE `x_news_comment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `NEWS` (`NEWS`);

--
-- Indexes for table `x_news_tags`
--
ALTER TABLE `x_news_tags`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `x_pages`
--
ALTER TABLE `x_pages`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `PARENT` (`PARENT`);

--
-- Indexes for table `x_poto_gal`
--
ALTER TABLE `x_poto_gal`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `x_produk`
--
ALTER TABLE `x_produk`
  ADD PRIMARY KEY (`PRODUK_ID`);

--
-- Indexes for table `x_testimoni`
--
ALTER TABLE `x_testimoni`
  ADD PRIMARY KEY (`TESTIMONI_ID`);

--
-- Indexes for table `x_users`
--
ALTER TABLE `x_users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `x_faq_produk`
--
ALTER TABLE `x_faq_produk`
  MODIFY `FAQ_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_fitur_produk`
--
ALTER TABLE `x_fitur_produk`
  MODIFY `FITUR_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_klien`
--
ALTER TABLE `x_klien`
  MODIFY `KLIEN_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_konten`
--
ALTER TABLE `x_konten`
  MODIFY `KONTEN_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_news`
--
ALTER TABLE `x_news`
  MODIFY `NEWS_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `x_news_category`
--
ALTER TABLE `x_news_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `x_news_comment`
--
ALTER TABLE `x_news_comment`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_news_tags`
--
ALTER TABLE `x_news_tags`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_pages`
--
ALTER TABLE `x_pages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `x_poto_gal`
--
ALTER TABLE `x_poto_gal`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_produk`
--
ALTER TABLE `x_produk`
  MODIFY `PRODUK_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_testimoni`
--
ALTER TABLE `x_testimoni`
  MODIFY `TESTIMONI_ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `x_users`
--
ALTER TABLE `x_users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
